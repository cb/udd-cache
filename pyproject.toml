# https://peps.python.org/pep-0517/
[build-system]
requires = ["setuptools>=45", "wheel"]
build-backend = "setuptools.build_meta"

# https://peps.python.org/pep-0621/
[project]
name = "udd-cache"
version = "0.4.2"
description = "Modules for querying the Ultimate Debian Database (UDD)"
readme = "README.md"
requires-python = ">=3.7"
license = {text = "BSD-3-Clause"}
authors = [
    {name = "Stuart Prescott", email = "stuart@debian.org"},
]
keywords = ["debian", "archive", "packages", "udd"]
dependencies = [
    "python-debian",
    "psycopg",
    "psycopg_pool",
]

[project.urls]
homepage = "https://salsa.debian.org/debian-irc-team/udd-cache/"

[project.scripts]
udd-cache = "uddcache.packages_cli:main"
udd-bts = "uddcache.bugs_cli:main"

# https://peps.python.org/pep-0621/#dependencies-optional-dependencies
[project.optional-dependencies]
test = [
    "pytest",
    "pytest-cov",
    "pytest-mock",
    "pytest-postgresql>=6.0.0",
]

# https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html
[tool.setuptools]
include-package-data = true
packages = [
    "uddcache",
    "uddcache.test",
]

[tool.setuptools.package-data]
"*" = [
    "py.typed",
    "*.pyi",
]
"uddcache" = [
    "udd-cache-public-mirror.conf",
    "debian.conf",
]
"uddcache.test" = [
    "data/*",
]

# https://docs.pytest.org/en/6.2.x/customize.html
[tool.pytest.ini_options]
minversion = "6.0"
addopts = "--cov-report term-missing --cov=uddcache  -v -rsx"
testpaths = ["uddcache/test"]
filterwarnings = [
]

# https://mypy.readthedocs.io/en/stable/config_file.html
[tool.mypy]
files = "uddcache/**/"
strict = true
disallow_any_generics = false
disallow_subclassing_any = false
show_error_codes = true
pretty = true
ignore_missing_imports = true
warn_unused_ignores = false

# https://coverage.readthedocs.io/en/6.4/config.html
[tool.coverage.report]
exclude_lines = [
    "pragma: no cover",
    "if TYPE_CHECKING:",
    "@overload",
    "except ImportError",
]

[tool.pylint."MESSAGES CONTROL"]
disable= '''
    fixme,
    invalid-name,
    line-too-long,
    locally-disabled,
    missing-function-docstring,
    missing-class-docstring,
    missing-module-docstring,
    too-few-public-methods,
    too-many-arguments,
    too-many-instance-attributes,
    too-many-positional-arguments,
    too-many-public-methods,
'''

[tool.pylint.REPORTS]
reports = false

[tool.pylint.SIMILARITIES]
min-similarity-lines = 999
