# udd-cache: Modules for querying the Ultimate Debian Database (UDD)

The Ultimate Debian Database, UDD, is a huge PostgreSQL database of information about Debian packages and bugs.

More information about UDD can be found on the
(Debian Wiki)[https://wiki.debian.org/UltimateDebianDatabase]
and on the
(UDD dashboard index)[https://udd.debian.org/].

This module is designed to provide programmatic access to the UDD.
The modules were originally crafted for the IRC bot "judd" to provide information about packages and bugs.
As part of the development of these tools, a command-line interface to the modules was also created.

# Requirements

This module requires access to a copy of UDD. This can be:
 - your own local version of UDD (most tables are not *that* hard to recreate yourself, or you can download a snapshot)
 - the UDD public mirror, accessible directly via TCP/IP.
 - Debian's own UDD instance running on udd.debian.org, either running on ullmann.d.o or by an ssh tunnel to a suitable machine.

Note that doing lots of SQL queries to a remote database over the internet is not particularly fast.

Once you have access to a suitable UDD, the following modules are needed either from Debian packages or from PyPI. The package is normally tested with the current Debian unstable release and also the current stable release.

For routine operation:

```
# apt install python3-psutil python3-psycopg python3-psycopg-pool postgresql-17 postgresql-client-17 postgresql-17-debversion
```

For testing:

```
# apt install python3-pytest python3-pytest-cov python3-pytest-mock
# pip install pytest-postgresql
```

Notes:

 - for `pip install` you should be working inside a venv, or you will need the `--break-system-packages` option, with the downsides that brings to your local machine.)
 - psycopg version 3 (not psycopg2) is used.


# Installation

From git, having installed the dependencies:

```
$ git clone https://salsa.debian.org/debian-irc-team/udd-cache
$ cd udd-cache
$ pip install .
```

Prior to search queries being made, you will need a config file to provide access to the database.
If no config file is specified, the public mirror of UDD will be used, as configured in `uddcache/udd-cache-public-mirror.conf`.


## Configuration

The configuration file must have a `[database]` section to provide access to the postgresql database for loading the data.

Configuration is loaded in the following order:
 1. `/etc/udd-cache.conf`  (system-wide settings)
 1. `~/.config/udd-cache.conf` (user settings)
 1. `./udd-cache.conf` (current 'task' settings from current working directory)
 1. file specified on command line
 1. file specified in the `UDD_CACHE_CONFIG` environment variable

The first three files are only read if they exist and it is not an error if they do not exist.
It is an error for explicitly specified files (environment, command line) to not exist.
The values in later files overwrite the values obtained from previous files.


# Command-line Usage: `udd-cache`

The module comes with two simple command-line interfaces for searches: `udd-cache` and `udd-bts`.

`udd-cache` is vaguely like the familiar `apt-cache` tool but for UDD. It has a number of subcommands as listed below.

All subcommands accept a
`--help` option that provides help text specific to that subcommand.




Most subcommands have options `--release` and `--arch` to filter the output or change the target for the query. These restrictions can also normally be appended to the command line:

```
$ udd-cache versions dpkg
Package: dpkg None/amd64
jessie:                                  1.17.27
jessie-security:                         1.17.26
stretch:                                 1.18.25
stretch-security:                        1.18.26
buster:                                  1.19.8
buster-security:                         1.19.8
bullseye:                                1.20.13
bullseye-security:                       1.20.10
bookworm:                                1.21.22
trixie:                                  1.22.6
sid:                                     1.22.7
```

```
$ udd-cache versions dpkg --release sid
Package: dpkg sid/amd64
sid:                                     1.22.7
```

```
$ udd-cache versions dpkg --release sid --arch arm64
Package: dpkg sid/arm64
sid:                                     1.22.7
```

```
$ udd-cache versions dpkg sid mips64el
Package: dpkg sid/mips64el
sid:                                     1.22.7
```


## `versions`: show package versions across releases

Like `apt-cache madison` or the `rmadison` utility, the `versions` subcommand gives an overview of the versions in the archive:

```
$ udd-cache versions dpkg
Package: dpkg None/amd64
jessie:                                  1.17.27
jessie-security:                         1.17.26
stretch:                                 1.18.25
stretch-security:                        1.18.26
buster:                                  1.19.8
buster-security:                         1.19.8
bullseye:                                1.20.13
bullseye-security:                       1.20.10
bookworm:                                1.21.22
trixie:                                  1.22.6
sid:                                     1.22.7
```

## `info`: show package info for a particular release

Like `apt-cache show`, the `info` subcommand shows package information for the target release and architecture.

```
$ udd-cache info dpkg
Package: dpkg, (admin, required)
Release: bookworm/amd64
Version: 1.21.22
Size: 1530.6k
Installed-Size: 6409k
Homepage: https://wiki.debian.org/Teams/Dpkg
Screenshot: https://screenshots.debian.net/package/dpkg
Description: Debian package management system
```

## `names`: search package names

The `names` subcommand searches package names, like `apt-cache search --names-only`.

The `*` character can be added as a wildcard.

```
$ udd-cache names dpkg*
dpkg* in bookworm/amd64:
dpkg 1.21.22
dpkg-awk 1.2+nmu2
dpkg-cross 2.6.20
dpkg-dev 1.21.22
dpkg-dev-el 37.9
dpkg-repack 1.52
dpkg-source-gitarchive 0.1.4
dpkg-www 2.64
```

## `archs`: show available architectures for a package

The `archs` subcommand lists the architectures (and versions) for each package.

```
$ udd-cache archs dpkg
dpkg: amd64 (1.21.22), arm64 (1.21.22), armel (1.21.22), armhf (1.21.22), i386 (1.21.22), mips64el (1.21.22), mipsel (1.21.22), ppc64el (1.21.22), s390x (1.21.22)
```

Version skew can be seen from when `mips64el` buildds were not keeping up:

```
$ udd-cache archs systemd --release sid
systemd: mips64el (255~rc3-2), amd64 (255~rc4-1), arm64 (255~rc4-1), armel (255~rc4-1), armhf (255~rc4-1), i386 (255~rc4-1), ppc64el (255~rc4-1), riscv64 (255~rc4-1), s390x (255~rc4-1)
```

## `rprovides`: reverse provides search

The `rprovides` subcommand searches for the packages that provide a virtual package. Synonym: `whatprovides`.

Note that reverse-provides searching is not fast.

A purely virtual package:

```
$ udd-cache rprovides mail-transport-agent
mail-transport-agent in bookworm/amd64 is provided by:
courier-mta
dma
esmtp-run
exim4-daemon-heavy
exim4-daemon-light
msmtp-mta
nullmailer
opensmtpd
postfix
sendmail-bin
ssmtp
```

A package that is both virtual and real:

```
$ udd-cache rprovides lzma
lzma in bookworm/amd64 is provided by:
xz-utils
lzma is also a real package.
```

A purely real package:

```
$ udd-cache rprovides dpkg
In bookworm/amd64, dpkg is a real package.
```

Versioned provides are also handled:

```
$ udd-cache rprovides gvim
gvim in bookworm/amd64 is provided by:
vim-gtk3 (= 2:9.0.1378-2)
vim-motif (= 2:9.0.1378-2)
```

```
$ udd-cache rprovides gvim --version 2:9.0.1378-2
gvim in bookworm/amd64 is provided by:
vim-gtk3 (= 2:9.0.1378-2)
vim-motif (= 2:9.0.1378-2)
```

```
$ udd-cache rprovides gvim --version foobar
No packages provide 'gvim (= foobar)' in bookworm/amd64.
```

## `provides`: what packages are provided by a real package

The `provides` subcommand shows the packages that are in the `Provides` field of the package. Versioned provides information is included.

```
$ udd-cache provides xz-utils
xz-utils in bookworm/amd64 provides: lzma.
```

```
$ udd-cache provides vim-gtk3
vim-gtk3 in bookworm/amd64 provides: editor, gvim (= 2:9.0.1378-2), vim (= 2:9.0.1378-2), vim-lua (= 2:9.0.1378-2), vim-perl (= 2:9.0.1378-2), vim-python3 (= 2:9.0.1378-2), vim-ruby (= 2:9.0.1378-2), vim-tcl (= 2:9.0.1378-2).
```

## `source`: which source package produced a binary package

The `source` subcommand shows which source package generated the specified binary package.

```
$ udd-cache source dpkg-dev
Source: dpkg
```

## `binaries`: list binary packages produced by a source package

The `binaries` subcommand lists all the binary packages created by the source package. Synonym `binary`. If the search term is not a source package but is a binary package, it will be first translated into a source package name using the specified architecture.
The list of binary packages includes those produced on all architectures; it is not filtered for the specified architecture.

```
$ udd-cache binaries dpkg
Binaries: dpkg, dpkg-dev, dselect, libdpkg-dev, libdpkg-perl
```

## `relations`: list all relationships for a package

The `relations` subcommand shows all the relationships that a package has declared: Depends, Recommends, Suggests, Enhances, Conflicts, Breaks, Replaces.
Each of these terms can be used to see only one of these fields: `depends`, `recommends`, `suggests`, `enhances`, `conflicts`, `breaks`, `replaces`. Note that Pre-Depends and Depends are concatenated.

```
$ udd-cache relations dpkg
Package: dpkg bookworm/amd64
Depends: tar (>= 1.28-1), libbz2-1.0, libc6 (>= 2.34), liblzma5 (>= 5.4.0), libmd0 (>= 0.0.0), libselinux1 (>= 3.1~), libzstd1 (>= 1.5.2), zlib1g (>= 1:1.1.4)
Recommends: None
Suggests: apt, debsig-verify
Enhances: None
Conflicts: None
Breaks: libapt-pkg5.0 (<< 1.7~b), lsb-base (<< 10.2019031300)
Replaces: None
```

```
$ udd-cache suggests dpkg
Package: dpkg bookworm/amd64
Suggests: apt, debsig-verify
```

## `builddeps`: listing build-dependencies

The `builddeps` subcommand shows the declared build-dependencies of the source package. Synonym: `build-deps`.

```
$ udd-cache builddeps debhelper
Package: debhelper bookworm/amd64
Build-Depends: dpkg-dev (>= 1.18.0~), libtest-pod-perl <pkg.debhelper.ci>, man-db <pkg.debhelper.ci>, perl:any, po4a
Build-Depends-Indep: None
```

## `recent`: show recent uploads of a package

The `recent` subcommand shows recent uploads of the package.
The *changer* is the signature block of the changelog (and in the .changes file), while the *signer* is the key that was used to sign the .changes file. NMUs are also flagged.

```
$ udd-cache recent python-debian
version              date       changer              signer               nmu
0.1.49               2022-11-20 Stuart Prescott      N/A
0.1.48               2022-10-03 Jelmer Vernoo=C4=B3  N/A
0.1.47               2022-08-27 Jelmer Vernooĳ       Jelmer Vernooĳ
0.1.46               2022-07-08 Jelmer Vernooĳ       Jelmer Vernooĳ
0.1.45               2022-07-05 Jelmer Vernooĳ       Jelmer Vernooĳ
0.1.44               2022-05-29 Stuart Prescott      Stuart Prescott
0.1.43               2022-01-16 Stuart Prescott      Stuart Prescott
0.1.42               2021-10-18 Stuart Prescott      Stuart Prescott
0.1.41               2021-09-19 Jelmer Vernooĳ       Jelmer Vernooĳ
0.1.40               2021-06-28 Stuart Prescott      Stuart Prescott
```

(Note that there are various issues with data extraction in UDD and so
mojibake as seen above or missing signed information is common.)


## `maint`: show maintainer information

The `maint` subcommand shows the maintainer information according to a specified uploaded version, defaulting to the most recent upload. Synonyms: `maintainer` `uploader`, `changer`.

```
$ udd-cache uploader python-debian
Version: 0.1.49
Date: 2022-11-20
Uploader: N/A <>
Changer: Stuart Prescott <stuart@example.net>
Maintainer: Debian python-debian Maintainers <pkg-python-debian-maint@lists.a=>
```

## `popcon`: show summary of popcon data

The `popcon` subcommand shows the Popularity Contest (popcon) data for a binary package.

```
$ udd-cache popcon dpkg
Popcon data for dpkg:
installed: 233190
vote:      212577
old:       3088
recent:    17481
nofiles:   44
```

## `checkdeps`: check that dependencies can be satisfied

The `checkdeps` subcommand iterates through the package dependencies (Pre-Depends+Depends, Recommends, Suggests) and checks to see if the packages are present.
This check is *not* undertaken recursively.
Virtual packages are taken into account when doing the check.

```
$ udd-cache checkdeps dpkg
Depends: satisfied
Recommends: satisfied
Suggests: satisfied
```

And for a package that needs to be recompiled against newer versions of libraries:

```
$ udd-cache checkdeps lives --release sid
Depends: unsatisfied: libavutil56 (>= 7:4.4), libswscale5 (>= 7:4.4)
Recommends: unsatisfied: youtube-dl
Suggests: satisfied
```

```
$ udd-cache checkdeps lives --release sid --type depends
Depends: unsatisfied: libavutil56 (>= 7:4.4), libswscale5 (>= 7:4.4)
```

## `checkinstall`: recursively check dependencies

The `checkinstall` subcommmand is a recursive version of the `checkdeps` subcommand. It looks at Pre-Depends/Depends and, optionally, Recommends. Package relations that could prevent installation such as Conflicts and Breaks are not checked.
The analysis can optionally include a list of which packages were obtained from each release.

```
$ udd-cache checkinstall dpkg
Depends:
Good: gcc-12-base, libacl1, libbz2-1.0, libc6, libgcc-s1, liblzma5, libmd0, libpcre2-8-0, libselinux1, libzstd1, tar, zlib1g
```

```
$ udd-cache checkinstall python3-debian
Depends:
  Good: dpkg, gcc-12-base, libacl1, libbz2-1.0, libc6, libcom-err2, libcrypt1, libdb5.3, libexpat1, libffi8, libgcc-s1, libgssapi-krb5-2, libk5crypto3, libkeyutils1, libkrb5-3, libkrb5support0, liblzma5, libmd0, libncursesw6, libnsl2, libpcre2-8-0, libpython3-stdlib, libpython3.11-minimal, libpython3.11-stdlib, libreadline8, libselinux1,libsqlite3-0, libssl3, libtinfo6, libtirpc-common, libtirpc3, libuuid1, libzstd1, media-types, python3, python3-chardet, python3-minimal, python3-pkg-resources, python3.11, python3.11-minimal, readline-common, tar, zlib1g
```

An example with packages that can't be installed:

```
udd-cache checkinstall lives --release sid
File "/home/stuart/.local/bin/udd-cache", line 8, in <module>
sys.exit(main())
^^^^^^
File "/srv/static/programming/debian/irc/bots/src/udd-cache/uddcache/clibase.py", line 83, in main
success = args.func(runner, args)
^^^^^^^^^^^^^^^^^^^^^^^
File "/srv/static/programming/debian/irc/bots/src/udd-cache/uddcache/packages_cli.py", line 1131, in <lambda>
func=lambda r, a: r.checkinstall(
^^^^^^^^^^^^^^^
File "/srv/static/programming/debian/irc/bots/src/udd-cache/uddcache/packages_cli.py", line 561, in checkinstall
solverh = self.dispatcher.checkInstall(
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
File "/srv/static/programming/debian/irc/bots/src/udd-cache/uddcache/database.py", line 133, in wrapper
return func(*args, **kwargs)
^^^^^^^^^^^^^^^^^^^^^
File "/srv/static/programming/debian/irc/bots/src/udd-cache/uddcache/package_queries.py", line 257, in checkInstall
solverh = relchecker.Check(package, withrecommends)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
File "/srv/static/programming/debian/irc/bots/src/udd-cache/uddcache/resolver.py", line 308, in Check
relation.status = self.Check(
^^^^^^^^^^^
File "/srv/static/programming/debian/irc/bots/src/udd-cache/uddcache/resolver.py", line 308, in Check
relation.status = self.Check(
^^^^^^^^^^^
File "/srv/static/programming/debian/irc/bots/src/udd-cache/uddcache/resolver.py", line 308, in Check
relation.status = self.Check(
^^^^^^^^^^^
[Previous line repeated 1 more time]
File "/srv/static/programming/debian/irc/bots/src/udd-cache/uddcache/resolver.py", line 321, in Check
relation.status = status
^^^^^^
UnboundLocalError: cannot access local variable 'status' where it is not associated with a value
```

## `checkbuilddeps`: check that build-dependencies can be satisfied

The `checkbuilddeps` subcommand is analogous to the `checkdeps` subcommand except that it operates on the Build-Depends and Build-Depends-Indep fields of the source package.

```
$ udd-cache checkbuilddeps python-debian
Build-dependency check for python-debian in bookworm/amd64:
Checked: bookworm
All build-dependencies satisfied.
bookworm: binutils, dh-python, python3-all, python3-apt, python3-chardet, python3-pytest, python3-setuptools, zstd
virtual: debhelper-compat (= 13) → debhelper
```


## `checkbackport`: check whether build-dependencies imply a simple backport

The `checkbackport` subcommand attempts to verify whether the source package might be a trivial backport - that is, whether the Build-Depends and Build-Depends-Indep can be satisfied in the target release.

```
$ udd-cache checkbackport python-debian --torelease oldstable
Backport check for python-debian in sid→bullseye/amd64:
Checked: bullseye, bullseye-backports
All build-dependencies satisfied.
bullseye: binutils, dh-python, python3-all, python3-apt, python3-chardet, python3-pytest, python3-setuptools, zstd
virtual: debhelper-compat (= 13) → debhelper debhelper
```


## `why`: find relationships between packages

The `why` subcommand is inspired by `aptitude why`, which provides a way to determine dependency pathways between two packages. It is useful in investigating why installing one package causes others to be installed.

It isn't fast, particularly if you are using a remote database.

Depends relationships are marked with ▶ and Recommends relationships are marked with ▷.

```console
$ udd-cache why reportbug python3-chardet
Packages reportbug and python3-chardet are linked by 2 chains.
reportbug ▶ python3-reportbug ▶ python3-debian ▶ python3-chardet
reportbug ▶ python3-reportbug ▶ python3-requests ▶ python3-chardet
```

```console
$ udd-cache why --with-recommends reportbug lsb-release
Packages reportbug and lsb-release are linked by 1 chains.
reportbug ▶ python3-reportbug ▶ python3-apt ▷ lsb-release
```



# Command-line Usage: `udd-bts`

The module comes with two simple command-line interfaces for searches: `udd-cache` and `udd-bts`.

`udd-bts` is vaguely like the familiar `bts` tool but works via UDD to obtain information about the Debian bug tracking system. It has a number of subcommands as listed below


All subcommands accept a
`--help` option that provides help text specific to that subcommand.

## `bug`: show bug information

The `bug` subcommand can be used to show information about an individual bug, information about a binary package, or information about a source package.

Show a bug by bug number:

```
$ udd-bts bug 718498
Bug: 718498
Package: python-debian
Source: python-debian
Severity: wishlist
Bts-Status: pending
Status: open
Opened: 2013-08-01
Last-Modified: 2013-08-01
Archived: False
Title: python-debian: switch ar implementation to python-arpy
```

Show bugs filed against binary package:

```
$ udd-bts bug python-debian
wishlist: 3: 672308, 718498, 795383
normal: 4: 585393, 686638, 710923, 891328
```

Show bugs filed against source package, prefixing with `src:`:
```
$ udd-bts bug src:python-debian
wishlist: 6: 672308, 718498, 795383, 931269, 961923, 1052154
normal: 7: 585393, 686638, 710923, 891328, 960278, 1029727, 1031674
```

Search bugs within a package by keyword:

```
$ udd-bts bug src:python-debian 822
#585393: Please be more robust against bogus data in a deb822 file
#710923: python-debian: deb822.GpgInfo doesn't handle multiple signatures
```


## `rm`: search for package removal bugs

The `rm` subcommand attempts to find bugs that match the removals pattern for the bug title:

```
$ udd-bts rm rust-exa
Bug: 1057336
Package: ftp.debian.org
Source: ftp.debian.org
Severity: normal
Bts-Status: done
Status: closed
Opened: 2023-12-03
Last-Modified: 2023-12-04
Archived: False
Title: RM: rust-exa -- ROM; replace by eza
```

## `wnpp`: search WNPP bugs

The "Work Needed and Prospective Packages" list in the BTS contains lists of requests for packages. The `wnpp` subcommand searches all request types, while the synonyms filter the request down to specific types:

 - `rfp`: request for packaging
 - `itp`: intent to package
 - `rfh`: request for help
 - `rfa`: request for adoption
 - `ita`: intent to adopt
 - `o`, `orphan`: orphaned packages

Only exact matches against the package name are accepted.

```
$ udd-bts wnpp apt-build
Bug: 365427
Package: wnpp
Source: wnpp
Severity: normal
Bts-Status: pending
Status: open
Opened: 2006-04-29
Last-Modified: 2022-09-17
Archived: False
Title: O: apt-build -- frontend to apt to build, optimize
```

```
$ udd-bts rfa latexdraw
Bug: 1028575
Package: wnpp
Source: wnpp
Severity: normal
Bts-Status: pending
Status: open
Opened: 2023-01-13
Last-Modified: 2023-01-13
Archived: False
Title: RFA: latexdraw -- vector drawing program for LaTeX using PSTricks
```

## `rfs`: package upload sponsorship requests

The `rfs` subcommand searches for sponsorship requests - bugs filed against the sponsorship-requests pseudopackage seeking assistance to upload packages.

```
$ udd-bts rfs ukui
Bug: 1049324
Title: RFS: ukui-system-appwidget/4.0.0.1-1 [ITP] -- Ukui time app widget
Severity: wishlist
Status: open
Opened: 2023-08-14
Last-Modified: 2023-08-15
Tags:
Submitter: example <example@example.net>
Owner:

Bug: 1051381
Title: RFS: ukui-interface/4.0.0.0-1 -- interface for system config and related libraries
Severity: normal
Status: open
Opened: 2023-09-07
Last-Modified: 2023-11-17
Tags:
Submitter: example <example@example.net>
Owner:
...
```

# Python Usage

From Python, the `kinfo.search` module contains functions for programmatic access to each of the above searches.

For example, the `pciid` subcommand above is based on the following code:

```python
import uddcache.udd

u = uddcache.udd.Udd()
bts = u.Bts()
bts.bug("987654")

print(b.package)
# python3-django-hyperkitty
print(b.title)
# python3-django-hyperkitty: Loads Google Fonts (fonts.gstatic.com), causing privacy breach
```

```python
import uddcache.udd

u = uddcache.udd.Udd()
release = u.BindRelease()
p = release.Package("python3-debian")

print(p.data["source"])
# python-debian
print(p.Depends())
# python3-chardet, python3:any

for dep in p.DependsList():
  print(dep, release.Package(dep[0].package).data["version"])
# python3-chardet 5.1.0+dfsg-2
# python3:any 3.11.2-1+b1
```

The functions in `uddcache.packages_cli`, `uddcache.package_queries`, `uddcache.bugs_cli` and `uddcache.bug_queries` provide useful templates for using the available tools.


## To-do list / limitations

 - build-dep filtering based on profiles?
 - Build-Depends-Arch not included in output
 - Build-Conflicts* not included in output
 - reverse-searches (rprovides, rdepends etc) are very slow as they have to wade through these text fields
 - searches that look for packages that don't exist are very slow because they become rprovides searches
 - removals, wnpp, and sponsorship-request bugs could be obtained from separate tables, potentially.
 - intent-to-salvage bugs?
