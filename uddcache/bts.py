#
# Ultimate Debian Database query tool
#
# Debian BTS abstraction layer
#
###
#
# Copyright (c) 2011-2021   Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

import datetime
import re

from typing import Any, Dict, Iterable, List, Optional, Union

from psycopg import sql
from psycopg.rows import dict_row

from .database import UddDatabase


severities = (
    "wishlist",
    "minor",
    "normal",
    "important",
    "serious",
    "grave",
    "critical",
)
all_severities = (
    "fixed",
    "wishlist",
    "minor",
    "normal",
    "important",
    "serious",
    "grave",
    "critical",
)

status = ("pending", "pending-fixed", "done", "forwarded", "fixed")
open_status = ("pending", "pending-fixed", "forwarded")
closed_status = ("done", "fixed")

wnpp_types = ("RFP", "ITP", "RFH", "RFA", "ITA", "O")


class Bugreport:
    def __init__(
        self, data: Optional[Dict[str, Any]] = None, archived: bool = False
    ) -> None:
        self.id: Optional[int] = None
        self.package: Optional[str] = None
        self.source: Optional[str] = None
        self.arrival: Optional[datetime.datetime] = None
        self.status: Optional[str] = None
        self.severity: Optional[str] = None
        self.submitter: Optional[str] = None
        self.submitter_name: Optional[str] = None
        self.submitter_email: Optional[str] = None
        self.owner: Optional[str] = None
        self.owner_name: Optional[str] = None
        self.owner_email: Optional[str] = None
        self.done: Optional[str] = None
        self.done_name: Optional[str] = None
        self.done_email: Optional[str] = None
        self.title: Optional[str] = None
        self.last_modified: Optional[datetime.datetime] = None
        self.forwarded: Optional[str] = None
        self.affects_oldstable: bool = False
        self.affects_stable: bool = False
        self.affects_testing: bool = False
        self.affects_unstable: bool = False
        self.affects_experimental: bool = False
        self.archived: bool = archived
        self.tags: List[str] = []
        if data:
            self.load(data)

    def load(self, data: Dict[str, Any]) -> None:
        cleaning_map = {
            "id": int,
            "affects_oldstable": bool,
            "affects_stable": bool,
            "affects_testing": bool,
            "affects_unstable": bool,
            "affects_experimental": bool,
            "archived": bool,
        }
        for field in data.keys():
            cleaner = cleaning_map.get(field, lambda x: x)
            setattr(self, field, cleaner(data[field]))  # type: ignore

    @property
    def readable_status(self) -> Optional[str]:
        if not self.status:
            return None
        if self.status in ("forwarded", "fixed"):
            return self.status
        if self.status == "done":
            return "closed"
        if self.status == "pending":
            return "open"
        if self.status == "pending-fixed":
            return "pending"
        # FIXME should this raise if it's not known?
        return None

    @property
    def wnpp_type(self) -> Optional[str]:
        if self.package != "wnpp":
            raise ValueError()
        if self.title is None:
            return None

        m = re.match(f"^({'|'.join(wnpp_types)}):? .*", self.title, re.I)
        if m:
            return m.group(1).upper()
        return None

    def __str__(self) -> str:
        s = [
            f"Bug: {self.id or ''}",
            f"Package: {self.package or ''}",
            f"Source: {self.source or ''}",
            f"Severity: {self.severity or ''}",
            f"Bts-Status: {self.status or ''}",
            f"Status: {self.readable_status or ''}",
            f"Opened: {self.arrival.date() if self.arrival else ''}",
            f"Last-Modified: {self.last_modified.date() if self.last_modified else ''}",
            f"Archived: {self.archived}",
        ]
        if self.tags:
            s.append(f"Tags: {', '.join(self.tags)}")
        if self.title:
            t = self.title.splitlines()
            if t:
                s.append(f"Title: {t[0]}")
        s.append("")

        return "\n".join(s)


class Bts:
    """interface to the UDD for extracting bug information"""

    def __init__(self, udd: UddDatabase, include_archived: bool = True) -> None:
        """create a BTS searching instance

        include_archived: include "archived" bugs in searches
        """
        self.udd = udd
        self.include_archived = include_archived

    def bugs(self, bugnumbers: Iterable[Union[str, int]]) -> List[Bugreport]:
        """look up one"""
        with self.udd.cursor(row_factory=dict_row) as c:
            cleanbugs: List[int] = []
            for b in bugnumbers:
                if isinstance(b, int):
                    cleanbugs.append(b)
                if isinstance(b, str):
                    b = b.replace("#", "")
                    cleanbugs.append(int(b))
            c.execute(
                r"""SELECT *, FALSE AS archived
                    FROM bugs
                    WHERE id = ANY(%(bugs)s)""",
                {"bugs": list(cleanbugs)},
            )
            found_bugs = [Bugreport(r) for r in c.fetchall()]
            if self.include_archived:
                found_bugnums = [b.id for b in found_bugs]
                missing_bugnums = tuple(n for n in cleanbugs if n not in found_bugnums)
                if missing_bugnums:
                    c.execute(
                        r"""SELECT *, TRUE AS archived
                            FROM archived_bugs
                            WHERE id = ANY(%(bugs)s)""",
                        {"bugs": list(missing_bugnums)},
                    )
                    found_bugs.extend([Bugreport(r) for r in c.fetchall()])
            return found_bugs

    def bug(self, bugnumber: Union[str, int]) -> Bugreport:
        b = self.bugs([bugnumber])
        if not b:
            raise BugNotFoundError(bugnumber)
        return b[0]

    def get_bugs(self, search: Dict[str, Any]) -> List[Bugreport]:
        """
        "package": bugs for the given package
        "source": bugs belonging to a source package
        "severity": bugs with a certain severity
        "status": tuple of desired statuses ('pending', 'pending-fixed',
                                            'done', 'forwarded', or 'fixed')
        "title": regular expression match on title
        """
        with self.udd.cursor(row_factory=dict_row) as c:
            wheres = {
                "package": sql.SQL("bugs_packages.package = %(package)s"),
                "source": sql.SQL("bugs_packages.source = %(source)s"),
                "severity": sql.SQL("severity = ANY(%(severity)s)"),
                "status": sql.SQL("status = ANY(%(status)s)"),
                "title": sql.SQL("title ~* %(title)s"),
            }
            where_sql = sql.SQL(" AND ").join(
                [wheres[s] for s in search if s in wheres]
            )
            sort_sql: sql.Composable = sql.SQL("")
            if "sort" in search:
                sort_sql = sql.SQL(" ORDER BY {sort} {sort_dir}").format(
                    sort=sql.Identifier(search["sort"]),
                    sort_dir=sql.SQL(
                        search["sort"] if search["sort"] == "DESC" else "ASC"
                    ),
                )
            limit_sql: sql.Composable = sql.SQL("")
            if "limit" in search:
                limit_sql = sql.SQL(" LIMIT {limit}").format(
                    limit=sql.Literal(int(search["limit"])),
                )
            q = self._query_builder(where_sql, sort_sql, limit_sql)
            # logger.debug("Bts.get_bugs: %s", q.as_string(c))
            c.execute(q, search)
            return [Bugreport(r) for r in c.fetchall()]

    def get_bugs_tags(self, bugs: Union[Bugreport, Iterable[Bugreport]]) -> None:
        """Look up the tags for a list of bug objects"""
        with self.udd.cursor() as c:
            archived_bugs: Dict[int, Bugreport] = {}
            unarchived_bugs: Dict[int, Bugreport] = {}

            if isinstance(bugs, Bugreport):
                self.get_bugs_tags([bugs])
                return

            for b in bugs:
                if b.id:
                    if b.archived:
                        archived_bugs[b.id] = b
                    else:
                        unarchived_bugs[b.id] = b

            if unarchived_bugs:
                c.execute(
                    r"""SELECT id, tag
                        FROM bugs_tags
                        WHERE id = ANY(%(bugs)s)""",
                    {"bugs": list(unarchived_bugs)},
                )
                for bugid, t in c.fetchall():
                    unarchived_bugs[bugid].tags.append(t)
            if archived_bugs:
                c.execute(
                    r"""SELECT id, tag
                        FROM archived_bugs_tags
                        WHERE id = ANY(%(bugs)s)""",
                    {"bugs": list(archived_bugs)},
                )
                for bugid, t in c.fetchall():
                    archived_bugs[bugid].tags.append(t)

    def _query_builder(
        self,
        where_sql: sql.Composable,
        sort_sql: sql.Composable,
        limit_sql: sql.Composable,
    ) -> sql.Composed:
        columns = sql.SQL(
            r"""
                        bugs.id,
                        bugs.package AS package,
                        bugs.source AS source,
                        arrival, status, severity,
                        submitter, submitter_name, submitter_email,
                        owner, owner_name, owner_email,
                        done, done_name, done_email, title,
                        last_modified, forwarded,
                        affects_oldstable, affects_stable, affects_testing,
                        affects_unstable, affects_experimental
                """
        )
        if self.include_archived:
            q = sql.SQL(
                r"""
                    SELECT {columns},
                        FALSE AS archived
                    FROM bugs
                    JOIN bugs_packages
                        ON bugs.id = bugs_packages.id
                    WHERE {where}
                UNION
                    SELECT {columns_archived},
                        TRUE AS archived
                    FROM archived_bugs AS bugs
                    JOIN archived_bugs_packages AS bugs_packages
                        ON bugs.id = bugs_packages.id
                    WHERE {where}
                {sort}
                {limit}
                """
            )
        else:
            q = sql.SQL(
                r"""
                    SELECT {columns},
                        FALSE AS archived
                    FROM bugs
                    JOIN bugs_packages
                        ON bugs.id = bugs_packages.id
                    WHERE {where}
                    {sort}
                    {limit}
                """
            )
        return q.format(
            columns=columns,
            columns_archived=columns,
            where=where_sql,
            sort=sort_sql,
            limit=limit_sql,
        )


class BugNotFoundError(LookupError):
    """Exception raised when a bug is assumed to exist but doesn't"""

    def __init__(self, bugnumber: Union[str, int]) -> None:
        super().__init__()
        self.bugnumber = bugnumber

    def __str__(self) -> str:
        return f"Bug number {self.bugnumber} was not found."
