# coding: utf-8

#
# Ultimate Debian Database query tool
#
# CLI bindings
#
###
# Copyright (c) 2010-2011  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Command line interface to udd - output to stdout """

# pylint: disable=too-many-lines

import argparse
from typing import cast, Any, Dict, Iterable, List, Optional, Union

from . import clibase
from . import package_queries
from .config import Config, FileLocationType
from . import packages
from . import relations as uddcacherelations
from . import udd
from . import __version__


class Cli(clibase.CliBase):
    """Run a specified command sending output to stdout"""

    # pylint: disable=too-many-public-methods

    def __init__(
        self,
        config: Optional[Union[FileLocationType, Config]] = None,
        options: Any = None,
        initialise: bool = True,
        uddInstance: Optional[udd.Udd] = None,
    ) -> None:
        super().__init__(
            config, options, initialise, package_queries.Commands, uddInstance
        )

    @staticmethod
    def notfound(
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        message: str = "No package named '%s' was found%s.",
    ) -> None:
        """print a message indicating that the package was not found"""
        if release:
            if arch:
                tag = f" in {release}/{arch}"
            else:
                tag = f" in {release}"
        else:
            if arch:
                tag = f" in {arch}"
            else:
                tag = ""
        print(message % (package, tag))

    def versions(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """look up the version of a package in a release or releases"""
        real_release = self.udd.data.clean_release_name(
            release, args=args, default=None
        )
        arch = self.udd.data.clean_arch_name(arch, args=args)

        try:
            pkgs = self.dispatcher.versions(package, real_release, arch)
        except packages.PackageNotFoundError:
            self.notfound(package, real_release, arch)
            return False

        replies = []
        for row in pkgs:
            if row["component"] == "main":
                origin = f"{row['release']}:"
            else:
                origin = f"{row['release']}/{row['component']}:"
            replies.append(f"{origin:40} {row['version']}")

        sep = "\n"
        print(f"Package: {package} {real_release}/{arch}\n{sep.join(replies)}")
        return True

    def info(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """show info (version, size, homepage, screenshot) about a package"""
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)

        try:
            p = self.dispatcher.info(package, release, arch)
        except packages.PackageNotFoundError:
            self.notfound(package, release, arch)
            return False

        print(f"Package: {package}, ({p['section']}, {p['priority']})")
        print(f"Release: {release}/{arch}")
        print(f"Version: {p['version']}")
        print(f"Size: {(p['size'] / 1024.0) if p['size'] else 0:0.1f}k")
        print(
            f"Installed-Size: {p['installed_size'] if p['installed_size'] else 0:.0f}k"
        )
        if p["homepage"]:
            print(f"Homepage: {p['homepage']}")
        if p["screenshot_url"]:
            print(f"Screenshot: {p['screenshot_url']}")
        print(f"Description: {p['description']}")
        return True

    def names(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """search for package names with wildcard (? and *) expressions"""
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)

        pkgs = self.dispatcher.names(package, release, arch)
        if not pkgs:
            self.notfound(
                package, release, arch, message="No packages matching %s were found%s."
            )
            return False

        replies = []
        for row in pkgs:
            if row["component"] == "main":
                replies.append(f"{row['package']} {row['version']}")
            else:
                replies.append(
                    f"{row['package']} {row['version']} ({row['component']})"
                )

        sep = "\n"
        print(f"{package} in {release}/{arch}:\n{sep.join(replies)}")
        return True

    def archs(
        self,
        package: str,
        release: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """
        Show for what architectures a package is available. By default,
        the current stable release is used.
        """
        release = self.udd.data.clean_release_name(release, args=args)

        try:
            pkgs = self.dispatcher.archs(package, release)
        except packages.PackageNotFoundError:
            self.notfound(package, release)
            return False

        replies = []
        for row in pkgs:
            replies.append(f"{row['architecture']} ({row['version']})")
        print(f"{package}: {', '.join(replies)}")
        return True

    def rprovides(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        version: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """
        Show the packages that 'Provide' the specified virtual package
        ('reverse provides').
        By default, the current stable release and amd64 are used.
        """
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)

        p = self.udd.BindPackage(package, release, arch)
        found = True
        if p.IsVirtual(version):
            sep = "\n"
            pkgver = [
                f"{pkg}{f' (= {ver})' if ver else ''}"
                for pkg, ver in p.ProvidersList(version)
            ]
            reply = f"{package} in {release}/{arch} is provided by:\n{sep.join(pkgver)}"
            if p.Found():
                reply += f"\n{package} is also a real package."
        else:
            if p.Found():
                reply = f"In {release}/{arch}, {package} is a real package."
            else:
                reply = f"No packages provide '{package}{f' (= {version})' if version else ''}' in {release}/{arch}."
                found = False

        print(reply)
        return found

    def provides(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """
        Show the list of "provided" packages for the specified binary package
        in the given release and architecture. By default, the current
        stable release and amd64 are used.
        """
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)

        p = self.udd.BindPackage(package, release, arch)

        if p.Found():
            # mypy doesn't realise that p.Found() means p.data is not None...
            assert p.data is not None
            if p.data["provides"]:
                print(f"{package} in {release}/{arch} provides: {p.data['provides']}.")
            else:
                print(f"{package} in {release}/{arch} provides no additional packages.")
            return True

        self.notfound(package, release, arch)
        return False

    def source(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """
        Show the name of the source package from which a given binary package
        is derived.
        By default, the current stable release and amd64 are used.
        """
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)

        r = self.udd.BindRelease(release, arch)

        try:
            p = r.bin2src(package)
        except packages.PackageNotFoundError:
            self.notfound(package, release, arch)
            return False

        print(f"Source: {p}")
        return True

    def binaries(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """
        Show the name of the binary package(s) that are derived from a given
        source package.
        By default, the current stable release is used.
        """
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)

        try:
            p = self.udd.BindSourcePackage(package, release, arch)
        except packages.PackageNotFoundError:
            self.notfound(package, release)
            return False

        print("Binaries:", ", ".join(p.Binaries()))
        return True

    def builddeps(
        self,
        package: str,
        release: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """
        Show the name of the binary packages on which a given source package
        or binary package build-depends.
        By default, the current stable release is used.
        """
        release = self.udd.data.clean_release_name(release, args=args)

        try:
            p = self.udd.BindSourcePackage(package, release)
        except packages.PackageNotFoundError:
            self.notfound(package, release)
            return False

        self._package_relation_lookup(p, release, "build_depends")
        self._package_relation_lookup(
            p, release, "build_depends_indep", skipErrors=True, skipHeaders=True
        )
        return True

    def relations(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """
        Show all package relationships for this binary package.
        By default, the current stable release is used.
        """
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)
        p = self.udd.BindPackage(package, release, arch)
        found = self._package_relation_lookup(p, release, "depends")

        if not found:
            return False

        self._package_relation_lookup(
            p, release, "recommends", skipErrors=True, skipHeaders=True
        )
        self._package_relation_lookup(
            p, release, "suggests", skipErrors=True, skipHeaders=True
        )
        self._package_relation_lookup(
            p, release, "enhances", skipErrors=True, skipHeaders=True
        )
        self._package_relation_lookup(
            p, release, "conflicts", skipErrors=True, skipHeaders=True
        )
        self._package_relation_lookup(
            p, release, "breaks", skipErrors=True, skipHeaders=True
        )
        self._package_relation_lookup(
            p, release, "replaces", skipErrors=True, skipHeaders=True
        )
        return True

    def depends(
        self,
        command: str,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """
        Show one particular package relationship for this binary package.
        By default, the current stable release is used.
        """
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)
        p = self.udd.BindPackage(package, release, arch)
        return self._package_relation_lookup(p, release, command)

    def recent(self, package: str) -> bool:
        """
        Show recent uploads of a package
        """
        release = self.udd.data.clean_release_name()
        try:
            p = self.udd.BindSourcePackage(package, release)
        except packages.PackageNotFoundError:
            self.notfound(package)
            return False

        uploads = self.dispatcher.uploads(p, limit=10)
        fmt = "%-20s %-10s %-20s %-20s %s"
        print(fmt % ("version", "date", "changer", "signer", "nmu"))
        for u in uploads:
            nmu = ["", "nmu"][u["nmu"]]
            print(
                fmt
                % (
                    u["version"],
                    u["date"].date(),
                    u["changed_by_name"],
                    u["signed_by_name"],
                    nmu,
                )
            )
        return True

    def maint(self, package: str, version: Optional[str] = None) -> bool:
        release = self.udd.data.clean_release_name()
        try:
            p = self.udd.BindSourcePackage(package, release)
            uploads = self.dispatcher.uploads(p, limit=1, version=version)
        except packages.PackageNotFoundError:
            self.notfound(package)
            return False

        u = uploads[0]
        print(f"Version: {u['version']}")
        print(f"Date: {u['date'].date()}")
        print(f"Uploader: {u['signed_by_name']} <{u['signed_by_email']}>")
        print(f"Changer: {u['changed_by_name']} <{u['changed_by_email']}>")
        print(f"Maintainer: {u['maintainer_name']} <{u['maintainer_email']}>")
        if u["nmu"]:
            print("NMU: yes")
        return True

    def popcon(self, package: str) -> bool:
        try:
            d = self.dispatcher.popcon(package)
        except packages.PackageNotFoundError:
            self.notfound(package)
            return False

        print(f"Popcon data for {package}:")
        print(f"  installed: {d['insts']}")
        print(f"  vote:      {d['vote']}")
        print(f"  old:       {d['olde']}")
        print(f"  recent:    {d['recent']}")
        print(f"  nofiles:   {d['nofiles']}")
        return True

    def checkdeps(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        deptypes: Optional[List[str]] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """
        Check a package's dependencies are satisfiable

        Checks that each dependency (by default, Depends, Recommends and
        Suggests) as listed by a package is satisfiable for the
        specified release and architecture. In contrast to the "checkinstall"
        function, this check is not done recursively.

        By default, the current stable release and amd64 are used.
        """
        # pylint: disable=unused-argument
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)

        relation = []
        if deptypes:
            for dep in deptypes:
                if dep in self.udd.data.relations:
                    relation.append(dep)
        if not relation:
            relation = self.udd.data.relations

        try:
            status = self.dispatcher.checkdeps(package, release, arch, relation)
        except packages.PackageNotFoundError:
            self.notfound(package, release, arch)
            return False

        allok = True
        for rel in self.udd.data.relations:
            if rel in relation:
                label = rel.title().replace("_", "-")
                if status[rel].bad:
                    print(f"{label}: unsatisfied: {status[rel].bad}")
                    allok = False
                else:
                    print(f"{label}: satisfied")
        return allok

    def checkbuilddeps(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        """
        Check a package's build-dependencies are satisfiable

        Checks that each Build-Depends and Build-Depends-Indep package
        as listed by a source package is satisfiable for the
        specified release and architecture.

        By default, the current stable release and amd64 are used.
        """
        # pylint: disable=unused-argument
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)

        r = self.udd.BindRelease(arch=arch, release=release)

        try:
            status: uddcacherelations.BuildDepStatus = self.dispatcher.checkBackport(
                package, r, r
            )
        except packages.PackageNotFoundError:
            self.notfound(package)
            return False

        print(f"Build-dependency check for {package} in {release}/{arch}:")
        print(f"Checked: {', '.join(r.release)}")

        self._builddeps_status_formatter(status)
        return status.AllFound()

    def checkinstall(
        self,
        package: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        withrecommends: bool = True,
        args: Optional[clibase.CommandArgsType] = None,
    ) -> bool:
        release = self.udd.data.clean_release_name(release, args=args)
        arch = self.udd.data.clean_arch_name(arch, args=args)

        try:
            solverh = self.dispatcher.checkInstall(
                package, release, arch, withrecommends
            )
        except packages.PackageNotFoundError:
            self.notfound(package, release, arch)
            return False

        flatlist = solverh.flatten()
        print(flatlist)
        if self.options.verbose:
            print(solverh)

        allok = len(solverh.depends.bad) == 0
        if withrecommends:
            allok &= len(solverh.recommends.bad) == 0
        return allok

    def checkbackport(
        self,
        package: str,
        arch: Optional[str] = None,
        fromrelease: Optional[str] = None,
        torelease: Optional[str] = None,
    ) -> bool:
        """
        Check that the build-dependencies listed by a package in the release
        specified as "from-release" are satisfiable for in "to-release" for the
        given host architecture.
        By default, a backport from unstable to the current stable release
        and amd64 are used.
        """
        fromrelease = self.udd.data.clean_release_name(
            fromrelease, default="unstable"  # FIXME make this distro dependent
        )
        torelease = self.udd.data.clean_release_name(
            torelease, default="stable"  # FIXME make this distro dependent
        )
        arch = self.udd.data.clean_arch_name(arch)

        fr = self.udd.BindRelease(arch=arch, release=fromrelease)
        # FIXME: should torelease do fallback to allow --to-release lenny-multimedia etc?
        releases = self.udd.data.list_dependent_releases(
            torelease, suffixes=["backports"]
        )

        pins = dict(zip(releases, reversed(range(len(releases)))))
        tr = self.udd.BindRelease(arch=arch, release=releases, pins=pins)

        try:
            status: uddcacherelations.BuildDepStatus = self.dispatcher.checkBackport(
                package, fr, tr
            )
        except packages.PackageNotFoundError:
            self.notfound(package)
            return False

        print(f"Backport check for {package} in {fromrelease}→{torelease}/{arch}:")
        print(f"Checked: {', '.join(tr.release)}")

        self._builddeps_status_formatter(status)
        return status.AllFound()

    def why(
        self,
        package1: str,
        package2: str,
        release: Optional[str] = None,
        arch: Optional[str] = None,
        withrecommends: bool = True,
    ) -> bool:
        """
        Find all the dependency chains that link two packages

        Generates a list of dependency chains that go from package1 to
        package2 in the specified release and architecture. Recommends
        are optionally included in the dependency analysis too. Note that
        this function will look for *all* dependency chains not just the
        shortest/strongest one that is available.
        """
        release = self.udd.data.clean_release_name(release)
        arch = self.udd.data.clean_arch_name(arch)

        try:
            chains = self.dispatcher.why(
                package1, package2, release, arch, withrecommends
            )
        except packages.PackageNotFoundError:
            self.notfound(package1, release, arch)
            return False

        if chains:
            print(
                f"Packages {package1} and {package2} are linked by {len(chains)} chains."
            )
            for c in chains:
                print(c)
            return True

        print(f"No dependency chain could be found between {package1} and {package2}")
        return False

    @classmethod
    def _builddeps_status_formatter(
        cls, status: uddcacherelations.BuildDepStatus
    ) -> None:
        if not status.AllFound():  # packages missing
            badrels = cls._builddeps_formatter(status, data="bad")
            print("Unsatisfiable build dependencies:")
            print("\n".join(badrels))
        else:
            extras = []
            rm: Dict[str, List[uddcacherelations.RelationshipOptions]] = (
                status.ReleaseMap()
            )
            for releasename in rm.keys():
                # print "XR=%s" % releasename
                l = []
                for relation in rm[releasename]:
                    if relation.archIgnore:
                        continue

                    # If satisfied, these these must exist
                    assert relation.satisfiedBy is not None
                    assert relation.satisfiedBy.packagedata is not None

                    if not relation.virtual:
                        # to keep mypy happy as it can't follow
                        assert relation.satisfiedBy.packagedata.data is not None
                        # print relation.package.data['package']
                        l.append(str(relation.satisfiedBy.packagedata.data["package"]))
                    else:
                        pkgver = [
                            pkg
                            for pkg, _ in relation.satisfiedBy.packagedata.ProvidersList(
                                relation.satisfiedBy.version
                            )
                        ]
                        providers = " ".join(pkgver)
                        provided_version = (
                            f" (= {relation.satisfiedBy.version})"
                            if relation.satisfiedBy.version
                            else ""
                        )
                        l.append(
                            f"{relation.satisfiedBy.package}{provided_version} → {providers}"
                        )
                #                extras.append("%s: %s" % (xr,
                #                    ", ".join([i.package.data['package'] for i in rm[xr]])))
                if l:
                    extras.append(f"{releasename}: {', '.join(l)}")
            print("All build-dependencies satisfied.")
            print("\n".join(extras))

    @classmethod
    def _package_relation_lookup(
        cls,
        package: packages.AbstractPackage,
        release: str,
        relation: str,
        skipErrors: bool = False,
        skipHeaders: bool = False,
    ) -> bool:
        """Print a summary of the relationships of a package"""
        label = relation.title().replace("_", "-")
        if not package.Found():
            if not skipErrors:
                cls.notfound(package.package, release, package.arch)
            return False
        if not skipHeaders:
            print(f"Package: {package.package} {release}/{package.arch}")
        print(f"{label}: {package.RelationEntry(relation)}")
        return True

    @staticmethod
    def _builddeps_formatter(
        bdstatus: uddcacherelations.BuildDepStatus, data: str = "bad"
    ) -> Iterable[str]:
        """Print a summary of the build-deps of a package"""

        def format_rel(
            rel: uddcacherelations.RelationshipOptionsList, longname: str
        ) -> Optional[str]:
            if rel:
                return f"{longname}: {rel}"
            return None

        l = [
            format_rel(bdstatus.bd.get(data), "Build-Depends"),
            format_rel(bdstatus.bdi.get(data), "Build-Depends-Indep"),
        ]
        return filter(None, l)

    @staticmethod
    def parse_args(argv: Optional[List[str]] = None) -> argparse.Namespace:
        # pylint: disable=too-many-statements

        description = """\
%(prog)s queries an Ultimate Debian Database instance to make it possible
to perform simple package-related queries."""

        parser = argparse.ArgumentParser(description=description, epilog="")

        # create sub commands for each import action to be performed
        commands = parser.add_subparsers(dest="command", required=True)

        # make some generic parsers for common arguments
        optional_release_parser = argparse.ArgumentParser(add_help=False)
        optional_release_parser.add_argument(
            "--release",
            action="store",
            metavar="RELEASE",
            help="release to search (default: stable)",
        )

        default_release_parser = argparse.ArgumentParser(add_help=False)
        default_release_parser.add_argument(
            "--release",
            action="store",
            default="stable",
            metavar="RELEASE",
            help="release to search (default: stable)",
        )

        optional_arch_parser = argparse.ArgumentParser(add_help=False)
        optional_arch_parser.add_argument(
            "--arch",
            "--architecture",
            action="store",
            metavar="ARCH",
            help="architecture to search (default: amd64)",
        )

        default_arch_parser = argparse.ArgumentParser(add_help=False)
        default_arch_parser.add_argument(
            "--arch",
            "--architecture",
            action="store",
            default="amd64",
            metavar="ARCH",
            help="architecture to search (default: amd64)",
        )

        package_parser = argparse.ArgumentParser(add_help=False)
        package_parser.add_argument(
            "package", action="store", metavar="PACKAGE", help="package to query"
        )

        # slurp up all remaining positional arguments and try to use them
        # note that this doesn't interact nicely with the default_*_parser
        # objects defined above
        arch_release_hoover_parser = argparse.ArgumentParser(add_help=False)
        arch_release_hoover_parser.add_argument(
            "details",
            action="store",
            metavar="DATA",
            nargs="*",
            help="additional release or architecture data",
        )

        with_recommends_parser = argparse.ArgumentParser(add_help=False)
        with_recommends_parser.add_argument(
            "--with-recommends",
            "--withrecommends",
            "-r",
            action="store_true",
            dest="withrecommends",
            default=False,
            help="include recommended packages in the dependency analysis",
        )

        cross_release_parser = argparse.ArgumentParser(add_help=False)
        cross_release_parser.add_argument(
            "--from-release",
            "--fromrelease",
            action="store",
            dest="fromrelease",
            default="unstable",  # FIXME: debian-specific
            help="select the origin release for the package",
        )
        cross_release_parser.add_argument(
            "--to-release",
            "--torelease",
            action="store",
            dest="torelease",
            default="stable",  # FIXME: debian-specific
            help="select the destination release for the package "
            "(a foo-backports release will automatically be included if it exists)",
        )

        ########### Subcommands ###########

        ## versions
        subparser = commands.add_parser(
            "versions",
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="list versions of a binary package in the archive; "
            "only the specified architecture is searched. "
            "Prefix the search term with src: "
            "to search source packages.",
        )
        subparser.set_defaults(
            func=lambda r, a: r.versions(a.package, a.release, a.arch, a.details)
        )

        ## info
        subparser = commands.add_parser(
            "info",
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="display basic package info about a package; like 'apt-cache show'",
        )
        subparser.set_defaults(
            func=lambda r, a: r.info(a.package, a.release, a.arch, a.details)
        )

        ## names
        subparser = commands.add_parser(
            "names",
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="search package names for a pattern (glob)",
        )
        subparser.set_defaults(
            func=lambda r, a: r.names(a.package, a.release, a.arch, a.details)
        )

        ## archs
        subparser = commands.add_parser(
            "archs",
            parents=[
                optional_release_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="list the architectures that have a binary package",
        )
        subparser.set_defaults(
            func=lambda r, a: r.archs(a.package, a.release, a.details)
        )

        ## rprovides
        subparser = commands.add_parser(
            "rprovides",
            aliases=["whatprovides"],
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="list which packages Provide a given package",
        )
        subparser.add_argument(
            "--version",
            action="store",
            metavar="VERSION",
            help="provided version to search (default: not versioned)",
        )
        subparser.set_defaults(
            func=lambda r, a: r.rprovides(
                a.package, a.release, a.arch, a.version, a.details
            )
        )

        ## provides
        subparser = commands.add_parser(
            "provides",
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="list the Provides of a given package",
        )
        subparser.set_defaults(
            func=lambda r, a: r.provides(a.package, a.release, a.arch, a.details)
        )

        ## source
        subparser = commands.add_parser(
            "source",
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="show which source package generated a binary package",
        )
        subparser.set_defaults(
            func=lambda r, a: r.source(a.package, a.release, a.arch, a.details)
        )

        ## binaries
        subparser = commands.add_parser(
            "binaries",
            aliases=["binary"],
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="show which binary package(s) are generated a source package",
        )
        subparser.set_defaults(
            func=lambda r, a: r.binaries(a.package, a.release, a.details)
        )

        ## builddeps
        subparser = commands.add_parser(
            "builddeps",
            aliases=["build-deps"],
            parents=[
                optional_release_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="show the build-dependencies of a package",
        )
        subparser.set_defaults(
            func=lambda r, a: r.builddeps(a.package, a.release, a.details)
        )

        ## relations
        subparser = commands.add_parser(
            "relations",
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="list the relationships of the specified package",
        )
        subparser.set_defaults(
            func=lambda r, a: r.relations(a.package, a.release, a.arch, a.details)
        )

        ## depends (etc)
        subparser = commands.add_parser(
            "depends",
            aliases=[
                "recommends",
                "suggests",
                "enhances",
                "conflicts",
                "breaks",
                "replaces",
            ],
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="list the relationships of the specified package",
        )
        subparser.set_defaults(
            func=lambda r, a: r.depends(
                a.command, a.package, a.release, a.arch, a.details
            )
        )

        ## recent
        subparser = commands.add_parser(
            "recent",
            parents=[package_parser],
            help="show information about recent uploads and who maintains a package; see also 'maint' that can query specific versions",
        )
        subparser.set_defaults(func=lambda r, a: r.recent(a.package))

        ## maint
        subparser = commands.add_parser(
            "maint",
            aliases=["maintainer", "changer", "uploader"],
            parents=[package_parser],
            help="show information about a particular package upload; see also 'recent' that can query all recent versions",
        )
        subparser.add_argument(
            "--version",
            action="store",
            dest="version_optional",
            metavar="VERSION",
            help="version to search (default: most recent)",
        )
        subparser.add_argument(
            "version_positional",
            action="store",
            metavar="VERSION",
            nargs="?",
            help="version to search (default: most recent)",
        )

        subparser.set_defaults(func=lambda r, a: r.maint(a.package, a.version))

        ## popcon
        subparser = commands.add_parser(
            "popcon",
            parents=[package_parser],
            help="show popcon data for the package",
        )
        subparser.set_defaults(func=lambda r, a: r.popcon(a.package))

        ## checkdeps
        subparser = commands.add_parser(
            "checkdeps",
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="check whether the relationships declared by the "
            "package can be fulfilled.",
        )
        subparser.add_argument(
            "--type",
            action="append",
            metavar="TYPE",
            dest="deptypes",
            choices=["depends", "recommends", "suggests"],
            help="version to search (default: most recent)",
        )
        subparser.set_defaults(
            func=lambda r, a: r.checkdeps(
                a.package, a.release, a.arch, a.deptypes, a.details
            )
        )

        ## checkbuilddeps
        subparser = commands.add_parser(
            "checkbuilddeps",
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                arch_release_hoover_parser,
            ],
            help="check whether the declared build-dependencies can be fulfilled.",
        )
        subparser.set_defaults(
            func=lambda r, a: r.checkbuilddeps(a.package, a.release, a.arch, a.details)
        )

        ## checkinstall
        subparser = commands.add_parser(
            "checkinstall",
            parents=[
                optional_release_parser,
                optional_arch_parser,
                package_parser,
                with_recommends_parser,
                arch_release_hoover_parser,
            ],
            help="check whether a package and all its dependencies are installable.",
        )
        subparser.set_defaults(
            func=lambda r, a: r.checkinstall(
                a.package, a.release, a.arch, a.withrecommends, a.details
            )
        )

        ## checkbackport
        subparser = commands.add_parser(
            "checkbackport",
            parents=[optional_arch_parser, package_parser, cross_release_parser],
            help="check whether a package is trivially backportable.",
        )
        subparser.set_defaults(
            func=lambda r, a: r.checkbackport(
                a.package, a.arch, a.fromrelease, a.torelease
            )
        )

        ## why
        subparser = commands.add_parser(
            "why",
            parents=[
                optional_release_parser,
                optional_arch_parser,
                with_recommends_parser,
            ],
            help="look for relationship links between two packages.",
        )
        subparser.add_argument(
            "package1", action="store", metavar="PKG1", help="starting package"
        )
        subparser.add_argument(
            "package2", action="store", metavar="PKG2", help="finishing package"
        )
        subparser.set_defaults(
            func=lambda r, a: r.why(
                a.package1, a.package2, a.release, a.arch, a.withrecommends
            )
        )

        # General options
        # No support for other distributions just yet, but in time...
        # parser.add_argument(
        # "--distro", dest='distro', default='debian',
        # help="the distribution to be queried in searches "
        # "(examples: --distro=debian --distro=ubuntu)"
        # )
        parser.add_argument(
            "--config",
            dest="config",
            metavar="FILE",
            default=None,
            help="the UDD database connection configuration file",
        )
        parser.add_argument(
            "-v",
            "--verbose",
            action="count",
            dest="verbose",
            default=0,
            help="include additional information in output",
        )
        parser.add_argument(
            "--version",
            action="version",
            version="%(prog)s " + str(__version__),
            help="print version string and exit",
        )

        class PackagesCliNamespace(argparse.Namespace):
            """parse_arg Namespace replacement to handle aliases

            This Namespace object combines the optional parameter form for
            --version=VERSION and the positional parameter VERSION varieties
            for the flexible CLI that has been defined.
            """

            _version: Optional[str] = None

            def _get_magic_version(self) -> Optional[str]:
                for n in ("_version", "version_optional", "version_positional"):
                    try:
                        v = cast(str, getattr(self, n, None))
                    except AttributeError:
                        pass
                    if v:
                        return v
                return None

            def _set_magic_version(self, v: str) -> None:
                self._version = v

            version = property(_get_magic_version, _set_magic_version)

        args = parser.parse_args(argv, PackagesCliNamespace())

        # No support for other distributions just yet, but in time...
        args.distro = "debian"
        return args


main = Cli.main
