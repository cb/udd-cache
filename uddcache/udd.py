#
# Ultimate Debian Database query tool
#
# Database abstraction layer
#
###
#
# Copyright (c) 2010-2021   Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

from typing import (
    Any,
    Dict,
    Optional,
)

from . import database
from . import packages
from . import bts

# bring the retry decorator into the udd namespace
# (the extra "as" is needed to keep mypy happy)
# pylint: disable=unused-import,useless-import-alias
from .database import database_retry as database_retry
from .database import MaxRetriesExceeded as MaxRetriesExceeded


class Udd(database.UddDatabase):
    """Representation of the UDD database

    Focus so far is:

    packages, source packages
        tables: packages, sources, upload_history, screenshots

    bugs
        tables: bugs, archived_bugs, bugs_tags, archived_bugs_tags

    See uddcache.database.UddDatabase for constructor details.
    """

    def BindRelease(
        self,
        release: packages.ReleaseSpecificationType = None,
        arch: str = "i386",
        **kwargs: Dict[str, Any],
    ) -> packages.Release:
        """
        Select a release from the database
        """
        if release is None:
            release = self.data.stable_release
        r = packages.Release(self, arch=arch, release=release, **kwargs)
        return r

    @database_retry()
    def BindPackage(
        self, package: str = "", release: Optional[str] = None, arch: str = "i386"
    ) -> packages.Package:
        """
        Select a package from the database
        """
        if release is None:
            release = self.data.stable_release
        r = packages.Release(self, arch=arch, release=release)
        p = r.Package(package)
        return p

    @database_retry()
    def BindSourcePackage(
        self,
        package: str = "",
        release: Optional[str] = None,
        arch: Optional[str] = None,
    ) -> packages.SourcePackage:
        """
        Select a source package from the database
        """
        if release is None:
            release = self.data.stable_release
        if arch is None:
            arch = self.data.default_arch
        r = packages.Release(self, release=release, arch=arch)
        p = r.Source(package)
        return p

    def Bts(self, include_archived: bool = True) -> bts.Bts:
        """
        Create a new bug tracker
        """
        return bts.Bts(self, include_archived)
