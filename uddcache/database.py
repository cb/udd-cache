###
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR PROFITS OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

""" Wrapper function psycopg database connections """

import contextlib
import functools
import logging
from pathlib import Path
import time
from typing import (
    Any,
    Callable,
    ContextManager,
    Generator,
    Optional,
    ParamSpec,
    Union,
    TypeVar,
)

import psycopg
import psycopg_pool

from .config import Config
from .data import DebianData


# maximum life of a connection (in seconds) before it should be
# renewed by putting back into the pool
conn_life_max = 100
# default max number of retries of database functions
default_max_retries = 3
# default time between retries on a connection (in milliseconds)
default_retry_wait_time = 100


def Connect(**kwargs: Any) -> psycopg_pool.ConnectionPool:
    """Create a psycopg database connection that logs all SQL

    kwargs: as per psycopg.connect()
    """
    conn = kwargs.get("_connection")
    if not conn:
        pool = psycopg_pool.ConnectionPool(
            kwargs=kwargs,
            min_size=2,
        )
    else:
        # provided connection (e.g. for testing), need a fake pool
        pool = FakeConnectionPool(conn)  # type: ignore

    return pool


class FakeConnectionPool:
    def __init__(self, psql: psycopg.Connection) -> None:
        self.psql = psql

    @contextlib.contextmanager
    def connection(self) -> Generator[psycopg.Connection, None, None]:
        yield self.psql

    def getconn(self) -> psycopg.Connection:
        return self.psql

    def putconn(self, conn: psycopg.Connection) -> None:
        pass

    def check(self) -> None:
        pass

    def close(self) -> None:
        pass

    def get_stats(self) -> dict[str, str]:
        return {}


class MaxRetriesExceeded(Exception):
    """Maximum number of retries of database operation exceeded"""


T = TypeVar("T")
P = ParamSpec("P")


def database_retry(
    max_retries: int = 0, wait_time: float = 0
) -> Callable[[Callable[P, T]], Callable[P, T]]:
    """retry function on database errors

    max_retries: number of times function will be retried
    wait_time: time (in ms) before retrying
    """

    def decorator(func: Callable[P, T]) -> Callable[P, T]:
        @functools.wraps(func)
        def wrapper(*args: P.args, **kwargs: P.kwargs) -> T:
            # apply defaults from module settings
            _max_retries = max_retries or default_max_retries
            _wait_time = wait_time or default_retry_wait_time

            for attempt in range(_max_retries):
                try:
                    return func(*args, **kwargs)
                except psycopg.OperationalError:
                    logging.info("Database has gone away: attempt %d", attempt)
                    time.sleep(_wait_time / 1000)
            logging.error("Database went away and didn't come back")
            raise MaxRetriesExceeded("Database has disappeared")

        return wrapper

    return decorator


class UddDatabase:
    """Representation of the UDD database

    Focus so far is:

    packages, source packages
        tables: packages, sources, upload_history, screenshots

    bugs
        tables: bugs, archived_bugs, bugs_tags, archived_bugs_tags
    """

    psql_pool: psycopg_pool.ConnectionPool
    conn: Optional[psycopg.Connection]
    conn_ts: float

    def __init__(
        self,
        config: Optional[Union[str, Path, Config]] = None,
        distro: str = "debian",
        db: Optional[psycopg.Connection] = None,
    ) -> None:
        """Initialise the connection to the UDD instance

        config: config filename or Config instance
        distro: string naming the distribution that is being used
        db: database handle (psycopg object); passing this overrides
                the use of the config parameter
        """
        if isinstance(config, (str, Path)) or config is None:
            config = Config(config)
        self.config = config

        # Find the database connection, either provided or by connecting
        self.conn_ts = 0
        self._connect(db)

        if distro == "debian":
            self.data = DebianData
        elif distro == "ubuntu":
            raise NotImplementedError("Only distro='debian' implemented")
            # TODO: add this in
        else:
            raise ValueError("Unknown data types requested by 'distro'")

    def __del__(self) -> None:
        self._disconnect()

    def _connect(self, _connection: Optional[psycopg.Connection] = None) -> None:
        arg_mapping = {
            "database": "dbname",
            "hostname": "host",
            "port": "port",
            "password": "password",
            "username": "user",
        }
        connection = {}
        c = self.config.db()
        for i in c.keys():
            if c[i] is not None:
                connection[arg_mapping[i]] = c[i]

        # make the connection
        self.psql_pool = Connect(
            _connection=_connection, client_encoding="UTF8", **connection
        )
        self.conn = None
        self.conn_ts = 0

    def _disconnect(self) -> None:
        if self.psql_pool:
            self.psql_pool.close()

    def _conn_life(self) -> float:
        return time.time() - self.conn_ts

    def connection(self) -> ContextManager[psycopg.Connection[Any]]:
        # we live with long-lived but flaky connections, so test it if it's old
        if not self.conn_ts or self._conn_life() > conn_life_max:
            self.psql_pool.check()
        return self.psql_pool.connection()

    @contextlib.contextmanager
    def cursor(self, **kwargs: Any) -> Generator[psycopg.Cursor[Any], None, None]:
        if not self.conn or self._conn_life() > conn_life_max:
            if self.conn:
                self.psql_pool.putconn(self.conn)
                self.psql_pool.check()
            self.conn = self.psql_pool.getconn()
            self.conn_ts = time.time()
        try:
            cur = None
            cur = self.conn.cursor(**kwargs)
            yield cur
        finally:
            if cur is not None:
                cur.close()

    def __enter__(self):
        # type: () -> UddDatabase
        return self

    def __exit__(self, exc_type: Any, exc_val: Any, exc_tb: Any) -> None:
        self._disconnect()
