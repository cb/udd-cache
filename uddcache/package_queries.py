#
# Ultimate Debian Database query tool
#
# Set piece queries of package information from the database
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

from typing import Any, Dict, Iterable, List, Optional, Union

from psycopg.rows import dict_row

from .udd import Udd, database_retry
from . import packages
from . import relations
from . import resolver


class Commands:
    def __init__(self, udd: Udd) -> None:
        self.udd = udd

    def _ping(self) -> List[Dict[str, str]]:
        """ping the database with no exception handling/retries"""
        with self.udd.cursor(row_factory=dict_row) as c:
            sql = "SELECT 1"
            c.execute(sql)
        return [{"1": "1"}]

    @database_retry()
    def versions(
        self, package: str, release: Optional[str], arch: str
    ) -> List[Dict[str, str]]:
        with self.udd.cursor(row_factory=dict_row) as c:
            if package.startswith("src:"):
                packagename = package[4:]
                sql = r"""SELECT DISTINCT s.release,s.version,s.component,r.sort
                        FROM sources s FULL OUTER JOIN releases r ON r.release = s.release
                        WHERE source=%(package)s"""
                if release:
                    sql += " AND s.release=%(release)s"
                sql += " ORDER BY r.sort"
            else:
                packagename = package
                sql = r"""SELECT DISTINCT p.release,p.version,p.component,r.sort
                        FROM packages p FULL OUTER JOIN releases r ON r.release = p.release
                        WHERE package=%(package)s AND
                            (architecture=%(arch)s OR architecture='all')"""
                if release:
                    sql += " AND p.release=%(release)s"

            sql += " ORDER BY r.sort"
            c.execute(
                sql,
                {
                    "package": packagename,
                    "arch": arch,
                    "release": release,
                },
            )

            pkgs = c.fetchall()
            if not pkgs:
                raise packages.PackageNotFoundError(package)

            return pkgs  # type: ignore

    @database_retry()
    def info(self, package: str, release: str, arch: str) -> List[Dict[str, str]]:
        with self.udd.cursor(row_factory=dict_row) as c:
            c.execute(
                r"""SELECT p.section, p.priority, p.version,
                        p.size, p.installed_size, p.description,
                        p.homepage, s.screenshot_url
                    FROM packages as p
                        LEFT JOIN screenshots as s ON p.package=s.package
                    WHERE p.package=%(package)s AND
                        (p.architecture=%(arch)s OR p.architecture='all') AND
                        p.release=%(release)s""",
                {
                    "package": package,
                    "arch": arch,
                    "release": release,
                },
            )

            pkg = c.fetchone()
            if not pkg:
                raise packages.PackageNotFoundError(package)
            return pkg  # type: ignore

    @database_retry()
    def names(self, package: str, release: str, arch: str) -> List[Dict[str, Any]]:
        """
        Search package names with * and ? as wildcards.
        """
        with self.udd.cursor(row_factory=dict_row) as c:
            packagesql = package.replace("*", "%")
            packagesql = packagesql.replace("?", "_")

            if package.startswith("src:"):
                packagesql = packagesql[4:]
                sql = r"""SELECT DISTINCT version, source AS package, component
                        FROM sources
                        WHERE source LIKE %(package)s AND
                            release=%(release)s
                        ORDER BY source"""
            else:
                sql = r"""SELECT DISTINCT version, package, component
                        FROM packages
                        WHERE package LIKE %(package)s AND
                            (architecture=%(arch)s OR architecture='all') AND
                            release=%(release)s
                        ORDER BY package"""

            c.execute(
                sql,
                {
                    "package": packagesql,
                    "arch": arch,
                    "release": release,
                },
            )
            return c.fetchall()  # type: ignore

    @database_retry()
    def archs(self, package: str, release: str) -> List[Dict[str, str]]:
        """
        Find in which architectures a package is available.
        """
        with self.udd.cursor(row_factory=dict_row) as c:
            c.execute(
                r"""SELECT architecture, version
                    FROM packages
                    WHERE package=%(package)s
                        AND release=%(release)s""",
                {"package": package, "release": release},
            )
            archs = c.fetchall()
            if not archs:
                raise packages.PackageNotFoundError(package)
            return archs  # type: ignore

    @database_retry()
    def uploads(
        self,
        package: Union[str, packages.AbstractPackage],
        version: str = "",
        limit: int = 0,
    ) -> List[Dict[str, Any]]:
        """
        Return the dates and versions of recent uploads of the specified source
        package.
        """
        with self.udd.cursor(row_factory=dict_row) as c:
            if isinstance(package, str):
                p = package
            else:
                p = package.package

            sql = r"""SELECT *
                        FROM upload_history
                        WHERE source=%(package)s"""
            if version:
                sql += """ AND version=%(version)s"""
            if limit:
                sql += """ ORDER BY date DESC LIMIT %(limit)s"""

            c.execute(
                sql,
                {
                    "package": p,
                    "version": version,
                    "limit": limit,
                },
            )
            ups = c.fetchall()
            if not ups:
                raise packages.PackageNotFoundError(package)
            return ups  # type: ignore

    @database_retry()
    def popcon(self, package: str) -> Dict[str, int]:
        """
        Return the popcon (popularity contest) data for the specified
        binary package.
        See also: http://popcon.debian.org/FAQ
        """
        with self.udd.cursor(row_factory=dict_row) as c:
            c.execute(
                r"""SELECT insts, vote, olde, recent, nofiles
                    FROM popcon
                    WHERE package=%(package)s""",
                {
                    "package": package,
                },
            )
            data = c.fetchone()
            if not data:
                raise packages.PackageNotFoundError(package)
            return data  # type: ignore

    @database_retry()
    def checkdeps(
        self, package: str, release: str, arch: str, rels: Iterable[str]
    ) -> Dict[str, relations.RelationshipStatus]:
        """
        Check that the dependencies listed by a package are satisfiable for the
        specified release and architecture.
        """
        releases = self.udd.data.list_dependent_releases(release)
        r = self.udd.BindRelease(arch=arch, release=releases)
        relchecker = resolver.Checker(r)

        statusdict = {}
        for rel in rels:
            # raises packages.PackageNotFoundError if package not found
            status = relchecker.Check(package, rel)
            statusdict[rel] = status
        return statusdict

    @database_retry()
    def checkInstall(
        self, package: str, release: str, arch: str, withrecommends: bool
    ) -> Optional[resolver.SolverHierarchy]:
        releases = self.udd.data.list_dependent_releases(release)
        r = packages.Release(self.udd, arch=arch, release=releases)
        relchecker = resolver.InstallChecker(r)
        # raises packages.PackageNotFoundError if package not found
        solverh = relchecker.Check(package, withrecommends)
        return solverh

    @database_retry()
    def checkBackport(
        self, package: str, fromrelease: packages.Release, torelease: packages.Release
    ) -> relations.BuildDepStatus:
        """
        Check that the build-dependencies listed by a package in the release
        specified as "fromrelease" are satisfiable for in "torelease" for the
        given host architecture.
        """
        relchecker = resolver.BuildDepsChecker(torelease)

        s = fromrelease.Source(package)
        # raises packages.PackageNotFoundError if package not found
        return relchecker.Check(s)

    @database_retry()
    def why(
        self,
        package1: str,
        package2: str,
        release: str,
        arch: str,
        withrecommends: bool,
    ) -> relations.DependencyChainList:
        """
        Find all the dependency chains between two packages

        Generates a list of dependency chains that go from package1 to
        package2 in the specified release and architecture. Recommends
        are optionally included in the dependency analysis too. Note that
        this function will look for *all* dependency chains not just the
        shortest/strongest one that is available.

        BUGS: Provides and optional dependencies ("a | b") are not handled
        except for accepting the first available package to satisfy them.

        BUGS: check that package2 exists before doing expensive work?
        """
        releases = self.udd.data.list_dependent_releases(release)
        r = packages.Release(self.udd, arch=arch, release=releases)
        relchecker = resolver.InstallChecker(r)
        # raises packages.PackageNotFoundError if package not found
        solverh = relchecker.Check(package1, withrecommends)

        assert solverh is not None

        chains = solverh.chains()
        chains = chains.truncated(package2).unique().sorted()
        return chains
