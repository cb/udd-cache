#
# Copyright (c) 2007,2008, Mike O'Connor
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###


""" Static classes describing the data that is available """

import configparser
from pathlib import Path

from typing import Any, Dict, Iterable, List, Optional, Tuple, Union, overload


class RelListComparison(list[str]):
    def __contains__(self, needle: Any) -> bool:
        def norm(s: str) -> str:
            return s.lower().replace("-", "_")

        vals = [norm(s) for s in self]
        needle = norm(needle)
        return needle in vals


class AvailableData:
    """Base class for available data across all distros"""

    data_filename: Path

    relations = RelListComparison(["depends", "recommends", "suggests"])

    extendedrelations = RelListComparison(relations[:])
    extendedrelations.append("enhances")
    extendedrelations.append("conflicts")

    def __init__(self, filename: Optional[Union[str, Path]] = None):
        if not filename:
            filename = self.data_filename

        config = configparser.ConfigParser(
            interpolation=configparser.ExtendedInterpolation()
        )
        config.read(filename)

        self.release_map: Dict[str, str] = {}
        self.releases: List[str] = []
        self.devel_release = ""
        self.stable_release = ""
        self.archs: List[str] = []
        self.default_arch: str = ""

        self.default_arch = config["metadata"]["default_arch"]
        self.stable_release = config["metadata"]["stable_release"]
        self.devel_release = config["metadata"]["devel_release"]

        for k, v in config.items("releases"):
            self.release_map[k] = v

        for release in config.get("known", "releases").splitlines():
            release = release.strip()
            if release:
                self.releases.append(release)

        for arch in config.get("known", "archs").splitlines():
            arch = arch.strip()
            if arch:
                self.archs.append(arch)

    @overload
    def clean_release_name(
        self,
        name: Optional[str] = None,
        optlist: Optional[Iterable[Tuple[str, str]]] = None,
        optname: str = "release",
        args: Optional[Iterable[str]] = None,
        default: str = "stable",
    ) -> str:
        pass  # pragma: no cover

    @overload
    def clean_release_name(
        self,
        name: Optional[str] = None,
        optlist: Optional[Iterable[Tuple[str, str]]] = None,
        optname: str = "release",
        args: Optional[Iterable[str]] = None,
        default: Optional[str] = None,
    ) -> Optional[str]:
        pass  # pragma: no cover

    def clean_release_name(
        self,
        name: Optional[str] = None,
        optlist: Optional[Iterable[Tuple[str, str]]] = None,
        optname: str = "release",
        args: Optional[Iterable[str]] = None,
        default: Optional[str] = "stable",
    ) -> Optional[str]:
        """
        Sanitised canonical release name
        """
        # Look for the name in an optlist, free-text args and simply specified
        rel = default
        optlist = optlist or []
        args = args or []
        for option, arg in optlist:
            if option == optname:
                rel = arg
        for arg in args:
            if arg in self.releases:
                rel = arg
        if name:
            rel = name
        # Sanitise the name
        if rel not in self.releases and rel not in self.release_map:
            rel = default
        if rel in self.release_map:
            rel = self.release_map[rel]
        return rel

    def clean_arch_name(
        self,
        name: Optional[str] = None,
        optlist: Optional[Iterable[Tuple[str, str]]] = None,
        optname: str = "arch",
        args: Optional[Iterable[str]] = None,
        default: str = "amd64",
    ) -> str:
        """
        Sanitised architecture name
        """
        # Look for the name in an optlist, free-text args and simply specified
        arch = default
        optlist = optlist or []
        args = args or []
        for option, arg in optlist:
            if option == optname:
                arch = arg
        for arg in args:
            if arg in self.archs:
                arch = arg
        if name:
            arch = name
        # Sanitise the name
        if arch not in self.archs:
            arch = default
        return arch

    def list_dependent_releases(
        self,
        release: str,
        suffixes: Optional[List[str]] = None,
        include_self: bool = True,
    ) -> List[str]:
        """
        List the releases that should also be included in the dependency
        analysis
        """
        rels: List[str] = []
        suffixes = suffixes or []
        clean_rel = self.clean_release_name(name=release, default=None)
        if not clean_rel:
            return rels

        if include_self:
            rels.append(clean_rel)
        parts = release.split("-")
        if len(parts) > 1:
            rel = self.clean_release_name(name=parts[0], default=None)
            if rel:
                rels.append(rel)
        for suffix in suffixes:
            rel = self.clean_release_name(name=f"{parts[0]}-{suffix}", default=None)
            if rel:
                rels.append(rel)
        if release == "experimental":  # FIXME: move to distro-specific
            rels.append("sid")
        return rels


DebianData = AvailableData(Path(__file__).parent / "debian.conf")
