###
# -*- coding: utf-8 -*-
#
# Copyright (c) 2007,2008, Mike O'Connor
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR PROFITS OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import itertools
import re

from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    List,
    Literal,
    Optional,
    Set,
    TypeVar,
    Union,
    TYPE_CHECKING,
    overload,
)

if TYPE_CHECKING:
    from . import packages  # pylint: disable=cyclic-import
    from . import resolver  # pylint: disable=cyclic-import


class Relationship:
    """Representation of a single package relationship

    Accepts either a textual representation of relationship or
    kwargs to describe the relationship. A set of these objects
    may form part of a RelationshipOptions object that represents
    a set of options in a declared relationship e.g. "Depends: foo | bar"

    relation: textual representation such as "foo", "foo (>> 1.2)",
                "foo (>> 1.2) [amd64]"
    package:  package name part of the relationship
    operator: operator part of the relationship (<<, <=, = >=, >>)
    version:  version part of the relationship
    arch:     architecture part of the relationship (either a list
                of valid architecture keywords e.g.
                ['amd64', 'i386'], or a space separated string of
                keywords e.g. 'amd64 i386')

    Typical usage:
        rel = Relationship(relation="libc6")
        rel = Relationship(relation="libc6 (>= 1.2.3)")
        rel = Relationship(package="libc6", operator=">=", version="1.2.3")
    """

    @overload
    def __init__(
        self,
        relation: str,
        package: Literal[None] = None,
        operator: Literal[None] = None,
        version: Literal[None] = None,
        arch: Literal[None] = None,
        archqual: Literal[None] = None,
    ) -> None:
        pass

    @overload
    def __init__(
        self,
        relation: Literal[None] = None,
        package: str = "",
        operator: Optional[str] = None,
        version: Optional[str] = None,
        arch: Optional[Union[List[str], str]] = None,
        archqual: Optional[str] = None,
    ) -> None:
        pass

    def __init__(
        self,
        relation: Optional[str] = None,
        package: Optional[str] = None,
        operator: Optional[str] = None,
        version: Optional[str] = None,
        arch: Optional[Union[List[str], str]] = None,
        archqual: Optional[str] = None,
    ) -> None:
        if relation and package:
            raise ValueError(
                "Cannot specify both the textual relation "
                "and the components as keyword arguments"
            )
        if (operator and not version) or (not operator and version):
            raise ValueError(
                "If one of 'operator' or 'version' keyword "
                "arguments is specified, both must be given"
            )
        if relation is None and package is None:
            raise ValueError("One of relation and package must be specified")

        self.relation: str
        """ textual representation of the entire relationship"""
        self.package: str
        """ package name from relationship as a string """
        self.operator: Optional[str]
        """ version operator from the relationship as a string """
        self.version: Optional[str]
        """ version string from the relationship as a string """
        self.arch: List[str]
        """ architecture specification for the relationship as a list """
        self.archIgnore: bool = False
        """ boolean: this relationship is to be ignored in this architecture"""
        self.archqual = archqual
        """ multi-arch qualifier """
        self.virtual: bool = False
        """ boolean: this relationship is satisfied only by virtual packages"""
        self.packagedata: "Optional[packages.Package]" = None
        """ For relations satisfied by real packages: the Package object; for
            "virtual"  relationships, a list of Providers"""

        if relation is not None:
            self.relation = relation
            self._ParseRelation(relation)
            assert self.package is not None
        elif package is not None:
            self.package = package
            self.operator = operator
            self.version = version
            self.arch = self._archsplit(arch)
            self.relation = ""
            self.relation = str(self)

        self._checkOperatorSyntax()

    def _ParseRelation(self, relationship: str) -> None:
        # print("checking item %s" % relationship)
        m = re.match(
            r"""(?x)
                  ^\s*
                  (?P<package>[\w\d.+-]+)
                  (:(?P<archqual>([a-zA-Z0-9][a-zA-Z0-9-]*))
                  )?
                  (?:
                    \s*
                    \(\s*
                      (?P<operator>\>\>|\>=|=|\<\<|\<=)\s*(?P<version>[^\s]+)
                    \s*\)
                  )?
                  (?:
                    \s*
                    \[\s*
                      (?P<arch>[^]]+)
                    \s*\]
                  )?
                  \s*
                  (
                    (?P<restrictions><.+>)
                  )?\s*
                  $
                  """,
            relationship,
        )
        if not m:
            raise ValueError("Couldn't parse the relationship expression")
        self.package = m.group("package")
        self.operator = m.group("operator")
        self.version = m.group("version")
        self.arch = self._archsplit(m.group("arch"))
        self.archqual = m.group("archqual")

    def isVersioned(self) -> bool:
        return self.operator is not None

    def _checkOperatorSyntax(self) -> None:
        ops = [">>", ">=", "=", "<=", "<<"]
        if self.operator and (not self.operator in ops):
            raise ValueError(f"Illegal operator found in relationship: {self.operator}")

    @staticmethod
    def _archsplit(value: Optional[Union[str, List[str]]]) -> List[str]:
        """split up whitespace separated into list of archs accept lists"""
        if isinstance(value, list):
            return value
        if value:
            return re.split(r"\s+", value.strip())
        return []

    def __str__(self) -> str:
        if self.relation:
            return self.relation
        s: List[str] = []
        if not self.archqual:
            s.append(self.package)
        else:
            s.append(f"{self.package}:{self.archqual}")
        if self.operator and self.version:
            s.append(f"({self.operator} {self.version})")
        if self.arch:
            s.append(f"[{' '.join(self.arch)}]")
        return " ".join(s)


class RelationshipOptions(list[Relationship]):
    """
    List of alternatives that form a single package relationship.

    Note: this will commonly contain only one entry in the list
    from a "Depends: foo" line generating a single entry for "foo".
    A line like "Depends: foo | bar" will create two entries in
    this list, one for each of "foo" and "bar".

    Typical usage:
    rel = RelationshipOptions("libc6")
    rel = RelationshipOptions("default-mta | mail-transport-agent")
    rel = RelationshipOptions("dpkg (>> 1.15.4)")
    """

    def __init__(self, options: str):
        """Create a list of Relationship objects that represent a
        single package relationship, parsing the standard
        'foo | bar' format for the option syntax.

        options: text representation of the relationship, for
            example any one of the following:
            'foo'
            'foo | bar'
            'foo (>> 1.2-3)'
        """
        self.relation = options  # text representation
        self.satisfiedBy: Optional[Relationship] = (
            None  # Relationship alternative that satisfies
        )
        self.satisfied = False  # relationship is satisfied (boolean)
        self.virtual = False  # used virtual package to satisfy
        # Extended status after running through a checker
        self.status: "Optional[Union[RelationshipStatus, resolver.SolverHierarchy]]" = (
            None
        )
        self.archIgnore: Optional[bool] = None
        super().__init__(self)
        for rel in self._SplitOptions(options):
            # print("found rel=%s" % rel)
            self.append(Relationship(relation=rel))

    @staticmethod
    def _SplitOptions(item: str) -> List[str]:
        return re.split(r"\s*\|\s*", item)

    def __str__(self) -> str:
        return self.relation


class RelationshipOptionsList(list[RelationshipOptions]):
    """
    List of RelationshipOptions objects

    Each RelationshipOptions object in the list is an individual relationship
    declared by the package (i.e. "libc6 (>= 2.3)" or "apache2 | httpd". This
    list of objects represents all of these relationships (e.g. the entire
    Depends entry for the package).
    """

    def ReleaseMap(self) -> Dict[str, List[RelationshipOptions]]:
        """
        Convert the list of relationship objects into a dictionary
        of releases with a list of package objects in each release

        Packages that have not been resolved by the resolver are
        added to a special "unresolved" release. Relationships
        that are resolved by virtual packages are placed into
        the "virtual" release. Dependencies that are irrelevant
        to the current architecture are mapped to the "archignore" release
        """
        releases: "Dict[str, List[RelationshipOptions]]" = {}
        for i in iter(self):
            # print("%s : %s" % (type(i), i))
            # map unresolved packages to a dummy release
            r = "unresolved"
            if i.satisfied:
                if i.archIgnore:
                    # put deps for the wrong arch somewhere else
                    # print("ARCHIGNORE DEP %s" % i)
                    r = "archignore"
                elif i.virtual:
                    # stick virtual relationships into a separate release too
                    r = "virtual"
                else:
                    # print("%s : %s" % (type(i), i))
                    # print("%s : %s" % (type(i.satisfiedBy), i.satisfiedBy))
                    # partly as debugging and partly for mypy
                    assert i.satisfiedBy is not None
                    assert i.satisfiedBy.packagedata is not None
                    assert i.satisfiedBy.packagedata.data is not None
                    r = i.satisfiedBy.packagedata.data["release"]
            if not r in releases:
                releases[r] = []
            releases[r].append(i)
        return releases

    def PackageSet(self) -> Set[str]:
        """Create a set representing the list but not containing duplicates

        The list of RelationshipOptions objects is scanned and the package that
        actually satisfied the relationship is added to the set. If the same
        package is requested multiple times then this process will de-duplicate
        the list.

        If the RelationshipOptions object has not been run through the resolver
        hence it is not know which option will actually satisfy the
        relationship, the raw relation is added to the set instead.
        FIXME: is this the desired behaviour?
        """
        d: Set[str] = set()
        for o in self:
            # print(o)
            if o.satisfiedBy:
                d.add(o.satisfiedBy.package)
            else:
                d.add(o.relation)
        return d

    def __str__(self) -> str:
        # print("making output %d" % len(self))
        return ", ".join([str(x) for x in self.__iter__()])


PkgDataType = TypeVar("PkgDataType", str, RelationshipOptionsList)
PkgDataStorageType = TypeVar(
    "PkgDataStorageType", List[str], Set[str], RelationshipOptionsList
)


class PackageLists(dict[str, PkgDataStorageType]):
    """
    Lists of packages as categorised into lists given pre-determined names.

    Example:
        l = PackageLists(['good', 'bad'])
        l.good.append('foo')
    """

    # FIXME: type annotations here aren't great. PkgDataStorageType needs to be covariant generic
    def __init__(
        self,
        fields: Iterable[str],
        generator: Callable[[], PkgDataStorageType] = list,  # type: ignore
    ) -> None:
        """construct the lists and the accessors to them

        fields: a list of field names (must be valid python variable names)
        generator: function to use to generate the lists
        """
        self._fields = fields
        for name in fields:
            self[name] = generator()

    def get(self, key: str) -> PkgDataStorageType:  # type: ignore
        v = super().get(key)
        assert v is not None
        return v  # type: ignore

    def set(self, name: str, value: PkgDataStorageType) -> None:
        self[name] = value

    def __getattr__(self, name: str) -> Any:
        if name in self.__dict__["_fields"]:
            return self[name]
        raise AttributeError(name)

    def __str__(self) -> str:
        def strline(label: str, rlist: PkgDataStorageType) -> Optional[str]:
            s = ", ".join(map(str, sorted(rlist)))
            if s:
                return f"{label}: {s}"
            return None

        s = [strline(name.title(), self.get(name)) for name in self._fields]
        return "\n".join(filter(None, s))

    def __len__(self) -> int:
        total = 0
        for field in self._fields:
            total += len(self.get(field))
        return total

    def __nonzero__(self) -> bool:
        return len(self) > 0


class RelationshipStatusSet(PackageLists[Set[str]]):
    good: Set[str]
    bad: Set[str]
    unchecked: Set[str]

    def __init__(self) -> None:
        super().__init__(["good", "bad", "unchecked"], set)


class RelationshipStatus(PackageLists[RelationshipOptionsList]):
    """Package lists reflecting whether dependencies are satisfied

    Typical usage:

    status = RelationshipStatus()
    status.good.extend(['perl', 'bash'])
    status.bad.extend(['python', 'ruby'])
    print(status.PackageSets)
    """

    good: RelationshipOptionsList
    bad: RelationshipOptionsList
    unchecked: RelationshipOptionsList

    def __init__(self) -> None:
        super().__init__(["good", "bad", "unchecked"], RelationshipOptionsList)

    def PackageSets(self) -> RelationshipStatusSet:
        """Return de-duplicated packages sets for the package lists

        Collapse the lists of package options in this object that may contain
        numerous requests for the same package perhaps due to various different
        packages in a dependency chain each requesting the package.

        The de-duplication of the lists is performed by converting them into
        python sets of Package objects.
        """
        s = RelationshipStatusSet()
        for r in self._fields:
            s.set(r, self.get(r).PackageSet())
        return s

    #    def CheckVersionedDeps(self):
    #        d = {}
    #        gops = [">>", ">="]?
    #            if d.has_key(o.satisfiedBy):
    #                v1 = d[o.satisfiedBy].version
    #                op1 = d[o.satisfiedBy].operator
    #                v2 = o.version
    #                op2 = o.operator
    #                if op1 in lops and op2 in lops:
    #
    #                elif op1 in gops and op2 in gops:
    #
    #                elif op1 == "=" and op2 == "=":
    #
    #                else:
    #
    #            else:
    #                d[o.satisfiedBy] = copy.deepcopy(o)
    #

    def extend(self, otherRelStatus: "RelationshipStatus") -> None:
        """Merge a second RelationshipStatus object into this one

        The contents of the given RelationshipStatus object are merged into
        this one, with each of the "good", "bad", and "unchecked" fields
        being extended by the given statuses.
        """
        for r in self._fields:
            self.get(r).extend(otherRelStatus.get(r))

    def swap(self) -> None:
        """Swap the good and bad entries around"""
        temp = self.bad
        self.bad = self.good
        self.good = temp

    def satisfied(self) -> bool:
        """Check if there are any 'bad' or 'unchecked' relationships"""
        # print("Satisfied?: %d %s" % (len(self.bad), len(self.bad)==0))
        return len(self.unchecked) + len(self.bad) == 0

    def __str__(self) -> str:
        """Convert to string form"""

        def strline(label: str, rlist: RelationshipOptionsList) -> Optional[str]:
            s = str(rlist)
            if s:
                return f"{label}: {s}"
            return None

        s = [
            strline("Good", self.good),
            strline("Bad", self.bad),
            strline("Unchecked", self.unchecked),
        ]
        ps: List[str] = []
        for p in self.good:
            # recurse into SolverHierarchy objects if they exist
            if p.status:
                ps.append(str(p.status))
        s.extend(ps)
        return "\n".join(filter(None, s))

    def __nonzero__(self) -> bool:
        """True (non-zero) if there are any packages in the list at all"""
        return len(self.good) + len(self.unchecked) + len(self.bad) > 0


class BuildDepStatus:
    """Contains the status of the build-dependencies of a package

    Both the Build-Depends and Build-Depends-Indep data can be found in this
    object.
    """

    def __init__(
        self,
        bd: Optional[RelationshipStatus] = None,
        bdi: Optional[RelationshipStatus] = None,
    ) -> None:
        self.bd = bd if bd is not None else RelationshipStatus()
        self.bdi = bdi if bdi is not None else RelationshipStatus()

    def AllFound(self) -> bool:
        """Check that all build-dependencies are satisified"""
        return self.bd.satisfied() and self.bdi.satisfied()

    def ReleaseMap(self) -> Dict[str, List[RelationshipOptions]]:
        """Prepare a mapping of which releases the packages come from

        When considering a collection of build-dependencies for a package,
        particularly in the case of backporting a package, map the packages
        back to the releases that would provide them to provide the requisite
        versioned build-deps.
        """
        bdm = self.bd.good.ReleaseMap()
        bdim = self.bdi.good.ReleaseMap()
        releases: Dict[str, List[RelationshipOptions]] = dict(
            (k, []) for k in itertools.chain(bdm.keys(), bdim.keys())
        )

        for r in bdm.keys():
            releases[r].extend(bdm[r])
        for r in bdim.keys():
            releases[r].extend(bdim[r])

        #        for r in m.keys():
        #            releases[r] = m[r]
        #        m = self.bdi.good.ReleaseMap()
        #        for r in m.keys():
        #            if not r in releases:
        #                releases[r] = []
        #            releases[r].extend(m[r])
        return releases

    def __str__(self) -> str:
        """Compile a string listing of the build-dependencies"""
        return f"Build-Depends: {self.bd}\nBuild-Depends-Indep: {self.bdi}\n"


class PackageRelationship:
    def __init__(self, packagedata: "packages.Package") -> None:
        self.packagedata = packagedata
        self._relation_marker = ""
        self._unicode_relation_marker = ""
        self.distance = 0

    def __lt__(self, other: "PackageRelationship") -> bool:
        return self.distance < other.distance

    def __str__(self) -> str:
        return f"{self._unicode_relation_marker}{self.packagedata.package}"


class Depends(PackageRelationship):
    def __init__(self, packagedata: "packages.Package") -> None:
        super().__init__(packagedata)
        self._relation_marker = "=>"
        self._unicode_relation_marker = " ▶ "  # u"⇒"
        self.distance = 1


class Recommends(PackageRelationship):
    def __init__(self, packagedata: "packages.Package") -> None:
        super().__init__(packagedata)
        self._relation_marker = "->"
        self._unicode_relation_marker = " ▷ "  # u"→"
        self.distance = 1000


class DependencyChain(list[PackageRelationship]):
    def __init__(
        self,
        relation: Optional[PackageRelationship] = None,
        chain: "Optional[DependencyChain]" = None,
        base: Optional[str] = None,
    ) -> None:
        super().__init__()
        if relation:
            self.append(relation)
        if chain:
            self.extend(chain)
        self.base = base

    def truncated(self, name: str) -> "DependencyChain":
        """returns a truncated list up to the specified package name"""
        newlist = DependencyChain()
        newlist.base = self.base
        if self.contains(name):
            for p in self:
                newlist.append(p)
                if p.packagedata.package == name:
                    break
        return newlist

    def contains(self, name: str) -> bool:
        """check if the list contains the specified package name"""
        # FIXME: can be written to bail out early
        return len([x for x in self if x.packagedata.package == name]) > 0

    def distance(self) -> int:
        x = 0
        for p in self:
            x += p.distance
        return x

    def __str__(self) -> str:
        return f"{self.base}{''.join(str(p) for p in self)}"


class DependencyChainList(list[DependencyChain]):
    def set_base(self, base: Optional[str]) -> None:
        for c in self:
            c.base = base

    def unique(self) -> "DependencyChainList":
        """return a de-duplicated list"""
        newlist = DependencyChainList()
        c = set()
        for p in self:
            pkey = str(p)
            if not pkey in c:
                newlist.append(p)
            c.add(pkey)
        return newlist

    def truncated(self, name: str) -> "DependencyChainList":
        """return a list of DependencyChains where the chain is truncated"""
        return DependencyChainList(
            [c.truncated(name) for c in self if c.contains(name)]
        )

    def sorted(self) -> "DependencyChainList":
        """return a list in order of relationship tightness"""
        L = [(chain.distance(), chain) for chain in self]
        L.sort()
        return DependencyChainList([chain for (distance, chain) in L])
