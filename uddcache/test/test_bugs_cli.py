# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

"""Unit test for cli.py"""

from typing import Generator
from unittest.mock import MagicMock

import pytest

from uddcache.bugs_cli import Cli
from uddcache.udd import Udd, MaxRetriesExceeded
from uddcache.test.test_clibase import TestCliBase, TestCliBaseParseArg


@pytest.fixture(name="cli")
def fixture_cli(udd: Udd) -> Generator[Cli, None, None]:
    class dummyOptions:
        def __init__(self) -> None:
            self.distro = "debian"
            self.verbose = False

    cli = Cli(options=dummyOptions(), uddInstance=udd)
    yield cli


class TestBugsCli(TestCliBase):
    def test_bug(self, cli: Cli) -> None:
        cli.options.verbose = False

        # test by bug number
        with self.assertInStdOut(
            ["Bug: 600000", "Package: test-package-all-releases", "Severity: wishlist"],
            ["Tags: patch"],
        ):
            assert cli.bug("600000")

        with self.assertInStdOut(["Bug: 600000"]):
            assert cli.bug("#600000")

        with self.assertInStdOut(["Sorry, bug 999999 was not found"]):
            assert not cli.bug("999999")

        # test by single search string (package search)
        with self.assertInStdOut(["Sorry, no bugs for"]):
            assert not cli.bug("999999999999")

        with self.assertInStdOut(["wishlist: 1: 600002"]):
            assert cli.bug("test-package-all-releases")

        with self.assertInStdOut(["critical: 1: 600004"]):
            assert cli.bug("test-package-different-name")

        # package exists but no bugs
        with self.assertInStdOut(["Sorry, no bugs"]):
            assert not cli.bug("test-suggests")

        # non-existent package
        with self.assertInStdOut(["Sorry, no bugs"]):
            assert not cli.bug("no-such-package")

        # keyword filter
        with self.assertInStdOut(["#600003: Confusingly", "#600004: Confusingly"]):
            assert cli.bug("test-package-different-name", "Confusingly")

        # Will also look up WNPP and RM bugs thus these are true
        with self.assertInStdOut(["RM: #500030"]):
            assert cli.bug("test-package-obsolete")

        with self.assertInStdOut(["O: #600011"]):
            assert cli.bug("test-orphaned-package")

        ################################
        ## Check some more verbose output
        cli.options.verbose = True

        with self.assertInStdOut(
            [
                "Bug: 600000",
                "Package: test-package-all-releases",
                "Severity: wishlist",
                "Tags: patch",
            ]
        ):
            assert cli.bug("600000")

        with self.assertInStdOut(["Bug: 600002"]):
            assert cli.bug("test-package-all-releases")

        with self.assertInStdOut(["Bug: 600004"]):
            assert cli.bug("test-package-different-name")

        with self.assertInStdOut(["Bug: 600004"]):
            assert cli.bug("src:test-source-package-different-name")

        # package exists but no bugs
        with self.assertInStdOut(["Sorry, no bugs"]):
            assert not cli.bug("test-suggests")

        # non-existent package
        with self.assertInStdOut(["Sorry, no bugs"]):
            assert not cli.bug("no-such-package")

        with self.assertInStdOut(["Sorry, no bugs"]):
            assert not cli.bug("src:no-such-package")

        # keyword filter
        with self.assertInStdOut(["Bug: 600003", "Bug: 600004"]):
            assert cli.bug("test-package-different-name", "Confusingly")

        with self.assertInStdOut(["Bug: 600003", "Bug: 600004"]):
            assert cli.bug("src:test-source-package-different-name", "Confusingly")

        # Will also look up WNPP and RM bugs thus these are true
        with self.assertInStdOut(["RM: #500030"]):
            assert cli.bug("test-package-obsolete")

        with self.assertInStdOut(["O: #600011"]):
            assert cli.bug("test-orphaned-package")

    def test_bug_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.bug("600000")

    def test_rcbugs(self, cli: Cli) -> None:
        with self.assertInStdOut(
            ["Package: test-package-very-buggy", "Bug: 600020", "Severity: serious"]
        ):
            assert cli.rcbugs("test-package-very-buggy")

        with self.assertInStdOut(
            ["Source: test-source-package-different-name", "Bug: 600004"]
        ):
            assert cli.rcbugs("test-package-different-name")

        with self.assertInStdOut(
            ["Package: test-package-different-name", "Bug: 600004"]
        ):
            assert cli.rcbugs("test-source-package-different-name")

        with self.assertInStdOut(["No release critical bugs were found"]):
            assert not cli.rcbugs("no-such-package")

    def test_rcbugs_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.rcbugs("test-package-very-buggy")

    def test_wnpp(self, cli: Cli) -> None:
        with self.assertInStdOut(["Bug: 600010"]):
            assert cli.wnpp("test-prospective-package", command="itp")

        with self.assertInStdOut(["Sorry, no RFP bug"]):
            assert not cli.wnpp("test-prospective-package", command="rfp")

        with self.assertInStdOut(["Bug: 600010"]):
            assert cli.wnpp("test-prospective-package", command="wnpp")

        with self.assertInStdOut(["Bug: 600011"]):
            assert cli.wnpp("test-orphaned-package", command="orphan")

        with self.assertInStdOut(["Sorry, no RFP bug"]):
            assert not cli.wnpp("no-such-package", command="rfp")

    def test_wnpp_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.wnpp("test-prospective-package", command="itp")

    def test_rm(self, cli: Cli) -> None:
        with self.assertInStdOut(["Bug: 500030"]):
            assert cli.rm("test-package-obsolete")

        with self.assertInStdOut(["Sorry, no removal bug"]):
            assert not cli.rm("no-such-package")

    def test_rm_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.rm("test-package-obsolete")

    def test_rfs(self, cli: Cli) -> None:
        # substring match, lots of matches
        with self.assertInStdOut(["Bug: 600015"]):
            assert cli.rfs("-")

        # exact match
        with self.assertInStdOut(["Bug: 600015"]):
            assert cli.rfs("test-sponsorship-request-package")

        # no match
        with self.assertInStdOut(["No open RFS bugs found"]):
            assert not cli.rfs("no-such-package")

    def test_rfs_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.rfs("test-sponsorship-request-package")


class TestBugsCliParseArgs(TestCliBaseParseArg):
    # Note that in testing the main() function below, the logic
    # is inverted as it is a command line utility, returning 0 on success,
    # non-zero on failure

    ##
    ## Test CLI runner with simple options
    ##

    def test_main(self, cli: Cli) -> None:  # type: ignore[override]    # pylint: disable=arguments-renamed
        self._test_general_main(cli)

    def test_main_bug(self, cli: Cli, udd: Udd) -> None:
        with self.assertInStdOut(["#600000:", "#600001"]):
            argv = ["bug", "test-package-all-releases", "buggy"]
            assert not cli.main(argv, uddInstance=udd)

        with self.assertInStdOut(["Bug: 600000", "Severity: wishlist"]):
            argv = ["bug", "600000"]
            assert not cli.main(argv, uddInstance=udd)

        argv = ["bug", "no-such-package"]
        assert cli.main(argv, uddInstance=udd)

    def test_main_rm(self, cli: Cli, udd: Udd) -> None:
        with self.assertInStdOut(["Bug: 500030"]):
            argv = ["rm", "test-package-obsolete"]
            assert not cli.main(argv, uddInstance=udd)

        argv = ["rm", "no-such-package"]
        assert cli.main(argv, uddInstance=udd)

    def test_main_wnpp(self, cli: Cli, udd: Udd) -> None:
        with self.assertInStdOut(["Bug: 600010"]):
            argv = ["wnpp", "test-prospective-package"]
            assert not cli.main(argv, uddInstance=udd)

        with self.assertInStdOut(["Sorry"]):
            argv = ["wnpp", "no-such-package"]
            assert cli.main(argv, uddInstance=udd)

    def test_main_rcbugs(self, cli: Cli, udd: Udd) -> None:
        with self.assertInStdOut(["Bug: 600020"]):
            argv = ["rcbugs", "test-package-very-buggy"]
            assert not cli.main(argv, uddInstance=udd)

        with self.assertInStdOut(["No release critical"]):
            argv = ["rcbugs", "no-such-package"]
            assert cli.main(argv, uddInstance=udd)

    def test_main_rfs(self, cli: Cli, udd: Udd) -> None:
        with self.assertInStdOut(["Bug: 600015"]):
            argv = ["rfs", "test-sponsorship-request-package"]
            assert not cli.main(argv, uddInstance=udd)

        with self.assertInStdOut(["No open"]):
            argv = ["rfs", "no-such-package"]
            assert cli.main(argv, uddInstance=udd)

    ##
    ## Test options parsing
    ##

    def test_parse_args(  # type: ignore[override]  # pylint: disable=arguments-renamed
        self, cli: Cli
    ) -> None:
        argv = ["--verbose", "bug", "some-package"]
        args = cli.parse_args(argv)
        assert args.verbose

        argv = ["--config", "somefile.conf", "bug", "some-package"]
        args = cli.parse_args(argv)
        assert args.config == "somefile.conf"

        argv = ["--version", "bug", "some-package"]
        with pytest.raises(SystemExit):
            args = cli.parse_args(argv)

    def test_bug(self, cli: Cli) -> None:
        argv = ["bug", "600000"]
        args = cli.parse_args(argv)
        assert args.command == "bug"
        assert args.search == "600000"
        assert args.keywords is None

        argv = ["bug", "some-package"]
        args = cli.parse_args(argv)
        assert args.command == "bug"
        assert args.search == "some-package"
        assert args.keywords is None

        argv = ["bug", "some-package", "mysearch"]
        args = cli.parse_args(argv)
        assert args.command == "bug"
        assert args.search == "some-package"
        assert args.keywords == "mysearch"

    def test_rm(self, cli: Cli) -> None:
        argv = ["rm", "some-package"]
        args = cli.parse_args(argv)
        assert args.command == "rm"
        assert args.package == "some-package"

    def test_wnpp(self, cli: Cli) -> None:
        argv = ["wnpp", "some-package"]
        args = cli.parse_args(argv)
        assert args.command == "wnpp"
        assert args.package == "some-package"

        for cmd in ["rfp", "itp", "ita", "orphan", "o"]:
            argv = [cmd, "some-package"]
            args = cli.parse_args(argv)
            assert args.command == cmd
            assert args.package == "some-package"

    def test_rfs(self, cli: Cli) -> None:
        argv = ["rfs", "some-package"]
        args = cli.parse_args(argv)
        assert args.command == "rfs"
        assert args.package == "some-package"

    def test_rcbugs(self, cli: Cli) -> None:
        argv = ["rcbugs", "some-package"]
        args = cli.parse_args(argv)
        assert args.command == "rcbugs"
        assert args.package == "some-package"

    def test_help(self, cli: Cli) -> None:
        for cmd in [
            "bug",
            "rm",
            "wnpp",
            "rfp",
            "itp",
            "ita",
            "orphan",
            "o",
            "rfs",
            "rcbugs",
        ]:
            argv = [cmd, "--help"]
            with pytest.raises(SystemExit):
                cli.parse_args(argv)
