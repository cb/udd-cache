# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

"""Unit test for uddconfig.py"""

import os
from typing import Generator

import pytest

from uddcache.config import Config
from uddcache.test.conftest import find_test_data_file


class TestConfig:
    @pytest.fixture
    def testconf(self) -> Generator[str, None, None]:
        filename = str(find_test_data_file("test_udd-cache.conf"))
        yield filename

    def testNoFileName(self) -> None:
        """Test no specified filename to load"""
        assert Config()
        with pytest.raises(ValueError):
            Config(skipDefaultFiles=True)

    def testGoodFileName(self, testconf: str) -> None:
        """Test loading of config file"""
        conf = Config(testconf)
        assert conf
        assert conf.db()["hostname"] == "udd.example.net"

    def testBadFileName(self) -> None:
        """Test loading of non-existent config file"""
        with pytest.raises(IOError):
            Config("/path/to/no/such/file")

    def testEnvironment(self, testconf: str) -> None:
        """Test loading config file from environment"""
        try:
            with pytest.raises(IOError):
                os.environ["UDD_CACHE_CONFIG"] = "/path/to/no/such/file"
                Config()

            os.environ["UDD_CACHE_CONFIG"] = testconf
            assert Config(skipDefaultFiles=True)

            with pytest.raises(ValueError):
                os.environ["UDD_CACHE_CONFIG"] = ""
                Config(skipDefaultFiles=True)

        finally:
            os.unsetenv("UDD_CACHE_CONFIG")

    def testConfDict(self) -> None:
        """Test loading config via a dict"""
        cd = {"database": "quux", "username": "foobar"}
        conf = Config(skipDefaultFiles=True, confdict=cd)
        assert conf
        assert conf.get("database", "database") == "quux"
        assert conf.get("database", "username") == "foobar"

    def testConfigGet(self, testconf: str) -> None:
        conf = Config(testconf)
        assert conf.get("database", "hostname", "fallback-value") == "udd.example.net"
        assert (
            conf.get("nosuchsection", "hostname", "fallback-value") == "fallback-value"
        )
        assert conf.get("database", "nosuchkey", "fallback-value") == "fallback-value"
