# Ultimate Debian Database query tool
#
# Database fixture for test suite
#
###
#
# Copyright (c) 2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Database fixture for UDD tests """

from collections import defaultdict
import email.utils
import os
from pathlib import Path
import unittest.mock

from typing import Any, Dict, Generator, List, Optional, Tuple

import pytest
from pytest_postgresql import factories
from pytest_mock import MockerFixture

from debian import deb822
import psycopg

from uddcache.udd import Udd
import uddcache.data
import uddcache.database


##########################################################################
# test files: these are deb822(5) format files that contain the database
# contents for the test suite. The functions below load them using
# python-debian's debian.deb822.Deb822 classes and load them into the
# test database. The test database itself is managed as a pytest fixture.


def find_test_data_file(filename: str) -> Path:
    """find a test file that is located within the test suite"""
    return Path(__file__).parent / "data" / filename


##########################################################################
# test suite database setup

setup_file = "test_setup.sql"


# Get rid of config for UDD access that is not wanted for testing
os.unsetenv("UDD_CACHE_CONFIG")


def load_data(**kwargs: Any) -> None:
    """load data into the database"""
    conn = psycopg.connect(**kwargs)
    PackagesDataLoader(conn)
    SourcesDataLoader(conn)
    PopconDataLoader(conn)
    ScreenshotDataLoader(conn)
    UploadHistoryDataLoader(conn)
    BugsDataLoader(conn)
    ArchivedBugsDataLoader(conn)


class DataLoader:
    columns: List[str]
    data_file: str
    table: str
    import_only_columns: List[str] = []

    def __init__(self, conn: psycopg.Connection) -> None:
        data = self.load()
        self.commit(data, conn)

    def load(self) -> List[Dict[str, Any]]:
        with open(find_test_data_file(self.data_file), "r", encoding="UTF-8") as fh:
            ds = []
            for pkg in deb822.Deb822.iter_paragraphs(fh):
                d = self.clean(pkg)
                ds.append(d)
        return ds

    def clean(self, raw: deb822.Deb822) -> Dict[str, Any]:
        d: Dict[str, Any] = defaultdict(lambda: None)
        for p in self.columns + self.import_only_columns:
            praw = p.replace("_", "-")
            if praw in raw:
                d[p] = raw[praw]
        return d

    def commit(self, ds: List[Dict[str, Any]], conn: psycopg.Connection) -> None:
        c = conn.cursor()
        cols, binds = self.make_tokens(self.columns)
        stmt = f"INSERT INTO {self.table} ({cols}) VALUES ({binds})"

        c.executemany(stmt, ds)
        conn.commit()

    @staticmethod
    def split_email(
        d: Dict[str, Any],
        col: str,
        colname: Optional[str] = None,
        colemail: Optional[str] = None,
    ) -> None:
        colname = colname or (col + "_name")
        colemail = colemail or (col + "_email")
        d[colname], d[colemail] = email.utils.parseaddr(d[col])

    @staticmethod
    def trueish(
        d: Dict[str, Any],
        col: str,
    ) -> None:
        d[col] = bool(d[col] and d[col].lower() in ["yes", "y", "true", "1"])

    @staticmethod
    def make_tokens(columns: List[str]) -> Tuple[str, str]:
        cols = ", ".join(columns)
        binds = ", ".join(f"%({c})s" for c in columns)
        return cols, binds


class PackagesDataLoader(DataLoader):
    columns = [
        "package",
        "version",
        "architecture",
        "maintainer",
        "maintainer_name",
        "maintainer_email",
        "description",
        "description_md5",
        "source",
        "source_version",
        "essential",
        "depends",
        "recommends",
        "suggests",
        "enhances",
        "pre_depends",
        "breaks",
        "installed_size",
        "homepage",
        "size",
        "build_essential",
        "origin",
        "sha1",
        "replaces",
        "section",
        "md5sum",
        "bugs",
        "priority",
        "tag",
        "task",
        "python_version",
        "ruby_versions",
        "provides",
        "conflicts",
        "sha256",
        "original_maintainer",
        "distribution",
        "release",
        "component",
        "multi_arch",
        "package_type",
    ]
    data_file = "test_Packages.822"
    table = "packages"

    def clean(self, raw: deb822.Deb822) -> Dict[str, Any]:
        d = super().clean(raw)

        for f in ["installed_size", "size"]:
            if d[f] is not None:
                d[f] = int(d[f])

        if "stable" in d["release"]:
            d["release"] = d["release"].replace(
                "stable", uddcache.data.DebianData.stable_release
            )

        # Source is non-mandatory, but we don't want it to be NULL
        if not d["source"]:
            d["source"] = d["package"]
            d["source_version"] = d["version"]
        else:
            split = d["source"].strip("'").split()
            if len(split) == 1:
                d["source_version"] = d["version"]
            else:
                d["source"] = split[0]
                d["source_version"] = split[1].strip("()")

        self.split_email(d, "maintainer")
        return d


class SourcesDataLoader(DataLoader):
    columns = [
        "source",
        "version",
        "maintainer",
        "maintainer_name",
        "maintainer_email",
        "format",
        "files",
        "uploaders",
        "bin",
        "architecture",
        "standards_version",
        "homepage",
        "build_depends",
        "build_depends_indep",
        "build_conflicts",
        "build_conflicts_indep",
        "priority",
        "section",
        "distribution",
        "release",
        "component",
        "vcs_type",
        "vcs_url",
        "vcs_browser",
        "python_version",
        "ruby_versions",
        "checksums_sha1",
        "checksums_sha256",
        "original_maintainer",
        "dm_upload_allowed",
        "testsuite",
        "autobuild",
        "extra_source_only",
    ]
    data_file = "test_Sources.822"
    table = "sources"

    def clean(self, raw: deb822.Deb822) -> Dict[str, Any]:
        d = super().clean(raw)

        d["source"] = raw["package"]

        if "stable" in d["release"]:
            d["release"] = d["release"].replace(
                "stable", uddcache.data.DebianData.stable_release
            )

        self.split_email(d, "maintainer")
        return d


class PopconDataLoader(DataLoader):
    columns = [
        "package",
        "insts",
        "vote",
        "olde",
        "recent",
        "nofiles",
    ]
    data_file = "test_Popcon.822"
    table = "popcon"


class ScreenshotDataLoader(DataLoader):
    columns = [
        "package",
        "version",
        "homepage",
        "maintainer_name",
        "maintainer_email",
        "description",
        "section",
        "screenshot_url",
        "large_image_url",
        "small_image_url",
    ]
    data_file = "test_Screenshots.822"
    table = "screenshots"

    def clean(self, raw: deb822.Deb822) -> Dict[str, Any]:
        d = super().clean(raw)

        self.split_email(d, "maintainer")
        return d


class UploadHistoryDataLoader(DataLoader):
    columns = [
        "source",
        "version",
        "date",
        "changed_by",
        "changed_by_name",
        "changed_by_email",
        "maintainer",
        "maintainer_name",
        "maintainer_email",
        "nmu",
        "signed_by",
        "signed_by_name",
        "signed_by_email",
        "key_id",
        "distribution",
        "file",
        "fingerprint",
    ]
    data_file = "test_Upload_History.822"
    table = "upload_history"

    def clean(self, raw: deb822.Deb822) -> Dict[str, Any]:
        d = super().clean(raw)

        self.trueish(d, "nmu")
        self.split_email(d, "changed_by")
        self.split_email(d, "maintained_by")
        self.split_email(d, "signed_by")
        return d


class BugsDataLoader(DataLoader):
    columns = [
        "id",
        "package",
        "source",
        "arrival",
        "status",
        "severity",
        "submitter",
        "submitter_name",
        "submitter_email",
        "owner",
        "owner_name",
        "owner_email",
        "done",
        "done_name",
        "done_email",
        "title",
        "last_modified",
        "forwarded",
        "affects_oldstable",
        "affects_stable",
        "affects_testing",
        "affects_unstable",
        "affects_experimental",
        "affected_packages",
        "affected_sources",
    ]
    import_only_columns = [
        "tags",
    ]
    data_file = "test_Bugs.822"
    table = "bugs"
    prefix = ""

    def clean(self, raw: deb822.Deb822) -> Dict[str, Any]:
        d = super().clean(raw)

        for boolcol in [
            "affects_oldstable",
            "affects_stable",
            "affects_testing",
            "affects_unstable",
            "affects_experimental",
        ]:
            self.trueish(d, boolcol)

        self.split_email(d, "submitter")
        self.split_email(d, "owner")
        self.split_email(d, "done")
        return d

    def commit(self, ds: List[Dict[str, Any]], conn: psycopg.Connection) -> None:
        super().commit(ds, conn)

        # also assemble the extra data for bugs
        c = conn.cursor()

        # bugs_packages (package, source, id mapping)
        cols, binds = self.make_tokens(["id", "package", "source"])
        stmt = f"INSERT INTO {self.prefix}bugs_packages ({cols}) VALUES ({binds})"
        c.executemany(stmt, ds)

        # bugs_tags (id, text) mapping
        alltags = []
        for d in ds:
            if d["tags"]:
                for tag in d["tags"].split():
                    alltags.append({"id": d["id"], "tag": tag})
        cols, binds = self.make_tokens(["id", "tag"])
        stmt = f"INSERT INTO {self.prefix}bugs_tags ({cols}) VALUES ({binds})"
        c.executemany(stmt, alltags)

        conn.commit()


class ArchivedBugsDataLoader(BugsDataLoader):
    data_file = "test_Archived_Bugs.822"
    table = "archived_bugs"
    prefix = "archived_"


##########################################################################
# Fixture setup for test suite

postgresql_templated_proc = factories.postgresql_proc(
    user="testudd",
    password="qwerty",
    dbname="testudd",
    port=None,
    load=[find_test_data_file(setup_file), load_data],
)

postgresql = factories.postgresql(
    "postgresql_templated_proc",
    dbname="testudd",
)


@pytest.fixture
def udd(request, postgresql):  # type: ignore
    # pylint: disable=redefined-outer-name
    request.cls.udd = Udd(db=postgresql)
    yield request.cls.udd


@pytest.fixture
def postgresql_direct_access(request, postgresql):  # type: ignore
    # pylint: disable=redefined-outer-name
    request.cls.postgresql = postgresql


@pytest.fixture
def stdout(request, capsys):  # type: ignore
    request.cls.capsys = capsys


@pytest.fixture
def mock_db_failure(
    mocker: MockerFixture,
) -> Generator[unittest.mock.MagicMock, None, None]:
    """test what happens when the database goes away"""
    exception_generator = mocker.MagicMock(
        side_effect=psycopg.OperationalError("Boom: simulating the db going away")
    )
    mocker.patch("psycopg.cursor.Cursor.execute", exception_generator)
    # mocker.patch('psycopg.cursor.Cursor.executemany', exception_generator)

    # reduce the wait time from (fake) retries in the testing
    uddcache.database.default_retry_wait_time = 1
    yield exception_generator
