# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Unit tests for generic UDD commands """

from typing import Generator
from unittest.mock import MagicMock

import psycopg
import pytest

from uddcache.udd import Udd, MaxRetriesExceeded
from uddcache.package_queries import Commands
from uddcache.packages import PackageNotFoundError
from uddcache.data import DebianData


class TestPackageCommands:
    @pytest.fixture
    def dispatcher(self, udd: Udd) -> Generator[Commands, None, None]:
        yield Commands(udd)

    def test_ping_success(self, dispatcher: Commands) -> None:
        """Test internal database ping"""
        # pylint: disable=protected-access
        dispatcher._ping()

    def test_ping_failure(
        self, dispatcher: Commands, mock_db_failure: MagicMock
    ) -> None:
        """Test internal database ping failure"""
        # pylint: disable=unused-argument
        with pytest.raises(psycopg.OperationalError):
            # pylint: disable=protected-access
            dispatcher._ping()

    def test_versions(self, dispatcher: Commands) -> None:
        """Test version lookups"""
        assert dispatcher.versions("test-package-all-releases", "sid", "amd64")
        assert dispatcher.versions("test-package-sid-armhf-only", "sid", "armhf")
        with pytest.raises(PackageNotFoundError):
            dispatcher.versions(
                "nosuchpackage",
                "sid",
                "amd64",
            )
        with pytest.raises(PackageNotFoundError):
            dispatcher.versions(
                "test-package-sid-armhf-only",
                "sid",
                "amd64",
            )
        assert dispatcher.versions("test-package-all-releases", None, "amd64")
        assert dispatcher.versions("src:test-package-all-releases", "sid", "amd64")
        assert dispatcher.versions(
            "src:test-source-package-different-name", None, "amd64"
        )
        with pytest.raises(PackageNotFoundError):
            dispatcher.versions(
                "src:nosuchpackage",
                "sid",
                "amd64",
            )

    def test_versions_retry(
        self, dispatcher: Commands, mock_db_failure: MagicMock
    ) -> None:
        """Test version lookups w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.versions(
                "src:nosuchpackage",
                "sid",
                "amd64",
            )
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.versions(
                "nosuchpackage",
                "sid",
                "amd64",
            )

    def test_info(self, dispatcher: Commands) -> None:
        """Test package information lookups"""
        assert dispatcher.info("test-package-all-releases", "sid", "amd64")
        with pytest.raises(PackageNotFoundError):
            dispatcher.info("nosuchpackage", "sid", "amd64")
        with pytest.raises(PackageNotFoundError):
            dispatcher.info(
                "test-package-sid-armhf-only",
                "sid",
                "amd64",
            )

    def test_info_retry(self, dispatcher: Commands, mock_db_failure: MagicMock) -> None:
        """Test package information w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.info(
                "nosuchpackage",
                "sid",
                "amd64",
            )

    def test_names(self, dispatcher: Commands) -> None:
        """Test package name lookups"""
        assert dispatcher.names("test-package-all-releases", "sid", "amd64")
        assert dispatcher.names("test-package-???-releases", "sid", "amd64")
        assert dispatcher.names("test-package-*", "sid", "amd64")
        assert dispatcher.names("*releases", "sid", "amd64")
        assert dispatcher.names("test*releases", "sid", "amd64")
        assert not dispatcher.names("test-package-sid-armhf-only", "sid", "amd64")
        assert not dispatcher.names("test-package-*-armhf-only", "sid", "amd64")
        assert dispatcher.names("test-package-sid-armhf-only", "sid", "armhf")
        assert dispatcher.names("test-package-sid-armhf-*", "sid", "armhf")
        assert dispatcher.names(
            "src:test-source-package-different-name", "sid", "amd64"
        )
        assert dispatcher.names("src:test-source-package-*-name", "sid", "amd64")
        assert not dispatcher.names("src:nosuchpackage", "sid", "amd64")

    def test_names_retry(
        self, dispatcher: Commands, mock_db_failure: MagicMock
    ) -> None:
        """Test package names w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.names(
                "nosuchpackage",
                "sid",
                "amd64",
            )

    def test_archs(self, dispatcher: Commands) -> None:
        """Test architecture availability lookups"""
        assert dispatcher.archs("test-package-all-releases", "sid")
        assert len(dispatcher.archs("test-package-all-releases", "sid")) == 2
        with pytest.raises(PackageNotFoundError):
            dispatcher.archs("nosuchpackage", "sid")

    def test_archs_retry(
        self, dispatcher: Commands, mock_db_failure: MagicMock
    ) -> None:
        """Test arch availability w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.archs(
                "nosuchpackage",
                "sid",
            )

    def test_popcon(self, dispatcher: Commands) -> None:
        """Test popcon data lookups"""
        # FIXME import popcon data
        p = dispatcher.popcon("test-package-all-releases")
        assert p
        assert p["insts"] == 42
        p = dispatcher.popcon("very-boring-package")
        assert p
        assert p["insts"] == 0
        with pytest.raises(PackageNotFoundError):
            dispatcher.popcon("nosuchpackage")

    def test_popcon_retry(
        self, dispatcher: Commands, mock_db_failure: MagicMock
    ) -> None:
        """Test popcon information w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.popcon("nosuchpackage")

    def test_uploads(self, dispatcher: Commands, udd: Udd) -> None:
        """Test upload/maintainer data lookups"""
        # FIXME import upload/maintainer data
        assert dispatcher.uploads("test-package-all-releases", limit=2)
        assert dispatcher.uploads("test-package-all-releases")
        assert dispatcher.uploads("test-package-all-releases", "1.2.2-7")
        with pytest.raises(PackageNotFoundError):
            dispatcher.uploads("no-such-package")
        with pytest.raises(PackageNotFoundError):
            dispatcher.uploads(
                "test-package-all-releases",
                "no-such-version",
            )
        with pytest.raises(PackageNotFoundError):
            dispatcher.uploads(
                "test-package-different-name"
            )  # only does source packages
        assert dispatcher.uploads("test-source-package-different-name")
        p = udd.BindSourcePackage("test-package-all-releases", "sid")
        assert dispatcher.uploads(p), "Check uploads with bind to source package failed"
        p = udd.BindSourcePackage("test-package-different-name", "sid")
        assert dispatcher.uploads(
            p
        ), "Check uploads with bind to source package via bin2src failed"

    def test_uploads_retry(
        self, dispatcher: Commands, mock_db_failure: MagicMock
    ) -> None:
        """Test package upload data w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.uploads("nosuchpackage")

    def test_checkdeps(self, dispatcher: Commands) -> None:
        """Test dependency testing for packages"""
        # packagename, [(relation, numbers)]
        checks = [
            ("test-package-all-releases", [("depends", 2)]),
            (
                "test-package-all-releases",
                [("depends", 2), ("recommends", 0), ("suggests", 0)],
            ),
            ("test-recommends", [("depends", 0), ("recommends", 1)]),
        ]

        for pkgname, rels in checks:
            d = dispatcher.checkdeps(pkgname, "sid", "amd64", [r[0] for r in rels])
            assert d, f"Dep Check for {pkgname}"
            for rel, num in rels:
                msg = f"Dep Check for {pkgname}[{rel}]"
                assert rel in d, msg
                if num is not None:
                    assert len(d[rel]) == num, msg

        with pytest.raises(PackageNotFoundError):
            dispatcher.checkdeps(
                "nosuchpackage",
                "sid",
                "amd64",
                ["depends"],
            )

    def test_checkdeps_retry(
        self, dispatcher: Commands, mock_db_failure: MagicMock
    ) -> None:
        """Test package information w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.checkdeps(
                "nosuchpackage",
                "sid",
                "amd64",
                ["depends"],
            )

    def test_checkinstall(self, dispatcher: Commands) -> None:
        """Test installability for packages"""
        # packagename, with-recommends, result
        checks = [
            ("test-package-all-releases", False, True),
            ("test-package-all-releases", True, True),
            ("test-depends-unsatisfied", False, False),
            ("test-depends-unsatisfied", True, False),
            ("test-recommends", False, True),
            ("test-recommends", True, True),
            ("test-recommends-unsatisfied", False, True),
            ("test-recommends-unsatisfied", True, False),
        ]

        for pkgname, withrec, result in checks:
            ci = dispatcher.checkInstall(pkgname, "sid", "amd64", withrec)
            msg = f"Install Check for {pkgname} (rec={withrec})"
            assert ci is not None, msg
            assert ci, msg
            summary = str(ci.flatten())
            if result:
                assert "Bad" not in summary, msg
            else:
                assert "Bad" in summary, msg

        with pytest.raises(PackageNotFoundError):
            dispatcher.checkInstall(
                "nosuchpackage",
                "sid",
                "amd64",
                True,
            )

    def test_checkinstall_retry(
        self, dispatcher: Commands, mock_db_failure: MagicMock
    ) -> None:
        """Test package information w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.checkInstall(
                "nosuchpackage",
                "sid",
                "amd64",
                True,
            )

    def test_why(self, dispatcher: Commands) -> None:
        """Test existence of package dependency chains"""
        # packagename1, packagename2, with-recommends, result
        checks = [
            ("test-package-all-releases", "libtesta1", False, True),
            ("test-package-all-releases", "libtesta1", True, True),
            (
                "test-package-all-releases",
                "no-such-package",
                False,
                False,
            ),  # should this raise?
            ("test-package-depends-multilevel", "libtestxd1", False, True),
            ("test-package-depends-multilevel", "libtestxd1", True, True),
            ("test-recommends", "libtesta1", False, False),
            ("test-recommends", "libtesta1", True, True),
            ("test-package-virtual-dependencies", "mail-transport-agent", True, True),
            # ("test-package-virtual-dependencies", "test-provides-mta-1", True, True),
            # ("test-package-virtual-dependencies", "test-provides-mta-2", True, True),
        ]
        # TODO: get past virtual packages with this?

        for pkgname1, pkgname2, withrec, result in checks:
            why = dispatcher.why(pkgname1, pkgname2, "sid", "amd64", withrec)
            msg = f"Why for {pkgname1} {pkgname2} (rec={withrec})"
            if result:
                assert why, msg
            else:
                assert not why, msg

        with pytest.raises(PackageNotFoundError):
            dispatcher.why(
                "no-such-package",
                "test-package-all-releases",
                "sid",
                "amd64",
                False,
            )

    def test_why_retry(self, dispatcher: Commands, mock_db_failure: MagicMock) -> None:
        """Test package information w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.why(
                "nosuchpackage",
                "nosuchpackage2",
                "sid",
                "amd64",
                False,
            )

    def test_checkbackport(self, dispatcher: Commands, udd: Udd) -> None:
        """Test 'simple sid backport' procedure on packages"""
        # TODO: it would be nice to actually test the accuracy of the tests
        fr = udd.BindRelease(arch="amd64", release="sid")
        tr = udd.BindRelease(arch="amd64")
        trbpo = udd.BindRelease(
            arch="amd64",
            release=[
                DebianData.stable_release,
                f"{DebianData.stable_release}-backports",
            ],
        )
        # FIXME self.udd.data.list_dependent_releases('squeeze', suffixes=['backports']))

        # packagename, torelease, result
        checks = [
            ("test-source-package-backportable-no-bpo", tr, True),
            ("test-source-package-backportable-no-bpo", trbpo, True),
            ("test-source-package-backportable-bpo", tr, False),
            ("test-source-package-backportable-bpo", trbpo, True),
            ("test-source-package-bd-virtual", tr, True),
        ]

        for pkgname, torelease, result in checks:
            cb = dispatcher.checkBackport(pkgname, fr, torelease)
            msg = f"Check Backport for {pkgname}"
            if result:
                assert cb.AllFound(), msg
            else:
                assert not cb.AllFound(), msg

        with pytest.raises(PackageNotFoundError):
            dispatcher.checkBackport("nosuchpackage", fr, tr)

    def test_checkbackport_retry(
        self, dispatcher: Commands, udd: Udd, mock_db_failure: MagicMock
    ) -> None:
        """Test package information w db failure"""
        # pylint: disable=unused-argument
        fr = udd.BindRelease(arch="amd64", release="sid")
        tr = udd.BindRelease(arch="amd64")
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.checkBackport("nosuchpackage", fr, tr)
