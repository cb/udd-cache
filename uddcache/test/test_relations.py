# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Unit tests for package relationships """

from typing import Generator, List, Tuple, no_type_check

import pytest

from uddcache.udd import Udd
from uddcache.relations import (
    Relationship,
    RelationshipOptions,
    RelationshipOptionsList,
    PackageLists,
    RelationshipStatus,
    BuildDepStatus,
    Depends,
    Recommends,
    DependencyChain,
    DependencyChainList,
)
from uddcache.resolver import Checker
from uddcache.data import DebianData


stable = DebianData.stable_release
stablebpo = f"{stable}-backports"


class TestRelationship:
    def testRelationship(self) -> None:
        """Test construction/parsing of package relationships"""
        r = Relationship(relation="pkg")
        assert r.package == "pkg" and not r.operator and not r.version and not r.arch
        r = Relationship(relation="pkg (>> 1.0.1)")
        assert (
            r.package == "pkg"
            and r.operator == ">>"
            and r.version == "1.0.1"
            and not r.arch
        )
        r = Relationship(relation="pkg (<< 1.0.1~)")
        assert (
            r.package == "pkg"
            and r.operator == "<<"
            and r.version == "1.0.1~"
            and not r.arch
        )
        r = Relationship(relation="pkg (>= 0)")
        assert (
            r.package == "pkg"
            and r.operator == ">="
            and r.version == "0"
            and not r.arch
        )
        r = Relationship(relation="pkg (= 1.2-3release4.1)")
        assert (
            r.package == "pkg"
            and r.operator == "="
            and r.version == "1.2-3release4.1"
            and not r.arch
        )
        r = Relationship(relation="pkg [amd64]")
        assert (
            r.package == "pkg"
            and not r.operator
            and not r.version
            and r.arch == ["amd64"]
        )
        r = Relationship(relation="pkg [!amd64]")
        assert (
            r.package == "pkg"
            and not r.operator
            and not r.version
            and r.arch == ["!amd64"]
        )
        r = Relationship(relation="pkg (>> 1.0) [amd64]")
        assert (
            r.package == "pkg"
            and r.operator == ">>"
            and r.version == "1.0"
            and r.arch == ["amd64"]
        )
        r = Relationship(relation="pkg:any (>> 1.0)")
        assert (
            r.package == "pkg"
            and r.operator == ">>"
            and r.version == "1.0"
            and r.archqual == "any"
        )

        r = Relationship(package="pkg", operator=">>", version="1.0", arch="amd64")
        assert (
            r.package == "pkg"
            and r.operator == ">>"
            and r.version == "1.0"
            and r.arch == ["amd64"]
        )
        r = Relationship(
            package="pkg", operator=">>", version="1.0", arch=["amd64", "i386"]
        )
        assert (
            r.package == "pkg"
            and r.operator == ">>"
            and r.version == "1.0"
            and r.arch == ["amd64", "i386"]
        )
        r = Relationship(package="pkg", operator=">>", version="1.0", arch="amd64 i386")
        assert (
            r.package == "pkg"
            and r.operator == ">>"
            and r.version == "1.0"
            and r.arch == ["amd64", "i386"]
        )

        # catch over-specification
        with pytest.raises(ValueError):
            Relationship(relation="pkg", package="pkg")  # type: ignore[call-overload]
        # catch invalid syntax
        with pytest.raises(ValueError):
            Relationship(package="pkg", operator=">>")
        with pytest.raises(ValueError):
            Relationship(package="pkg", version="2.0")
        with pytest.raises(ValueError):
            Relationship(relation="pkg >> 1.0")
        with pytest.raises(ValueError):
            Relationship(package="pkg", operator="!=", version="1.0")
        with pytest.raises(ValueError):
            Relationship(relation="pkg != 1.0")

    def testStr(self) -> None:
        r = Relationship(package="pkg", operator=">>", version="1.0")
        assert str(r) == "pkg (>> 1.0)"
        r = Relationship(package="pkg", operator=">>", version="1.0", arch="amd64")
        assert str(r) == "pkg (>> 1.0) [amd64]"
        r = Relationship(
            package="pkg", operator=">>", version="1.0", arch=["amd64", "i386"]
        )
        assert str(r) == "pkg (>> 1.0) [amd64 i386]"
        r = Relationship(relation="pkg (>> 1.1) [386]")
        assert str(r) == "pkg (>> 1.1) [386]"

    def testIsVersioned(self) -> None:
        r = Relationship(package="pkg", operator=">>", version="1.0")
        assert r.isVersioned()
        r = Relationship(package="pkg")
        assert not r.isVersioned()


class TestRelationshipOptions:
    def testRelationshipOptions(self) -> None:
        """Test parsing of options in package relationship clauses"""
        o = RelationshipOptions("a")
        assert o[0].package == "a" and len(o) == 1
        o = RelationshipOptions("a | b")
        assert o[0].package == "a" and o[1].package == "b" and len(o) == 2
        o = RelationshipOptions("a | b | c")
        assert (
            o[0].package == "a"
            and o[1].package == "b"
            and o[2].package == "c"
            and len(o) == 3
        )
        o = RelationshipOptions("a (>> 1.0)")
        assert o[0].package == "a" and o[0].version == "1.0" and len(o) == 1
        o = RelationshipOptions("a (>> 1.0 ) | b (= 2.0)")
        assert (
            o[0].package == "a"
            and o[0].version == "1.0"
            and o[1].package == "b"
            and o[1].version == "2.0"
        )


@pytest.mark.usefixtures("udd")
class TestRelationshipOptionsList:
    udd: Udd

    def testReleaseMap(self) -> None:
        """Test mapping a list of RelationshipOptions into a releases/packages dict"""
        # pylint: disable=too-many-statements

        # non-existent package should be unresolvable
        rl = RelationshipOptionsList()
        rl.append(RelationshipOptions("no-such-package"))
        assert rl
        assert rl.ReleaseMap()["unresolved"]

        # common checker for following tests
        release = self.udd.BindRelease(arch="amd64", release="sid")
        checker = Checker(release)

        # package where everything is resolvable
        p = release.Package("test-package-depends-single-level-a")
        rl = p.RelationshipOptionsList("depends")
        assert rl
        assert rl.ReleaseMap()["unresolved"]
        co = checker.CheckRelationshipOptionsList(rl)
        assert co.good.ReleaseMap()
        assert co.good.ReleaseMap()["sid"]
        assert not "unresolved" in co.good.ReleaseMap()

        # package where everything is resolvable (multiple deps)
        p = release.Package("test-package-depends-single-level-b")
        rl = p.RelationshipOptionsList("depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert co.good.ReleaseMap()
        assert co.good.ReleaseMap()["sid"]
        assert not "unresolved" in co.good.ReleaseMap()

        # package where everything is resolvable multilevel deps
        p = release.Package("test-package-depends-multilevel")
        rl = p.RelationshipOptionsList("depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert co.good.ReleaseMap()
        assert co.good.ReleaseMap()["sid"]
        assert not "unresolved" in co.good.ReleaseMap()

        # package with resolvable option groups
        p = release.Package("test-package-depends-single-level-options")
        rl = p.RelationshipOptionsList("depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert co.good.ReleaseMap()
        assert co.good.ReleaseMap()["sid"]
        assert not "unresolved" in co.good.ReleaseMap()

        # package with unresolvable option groups
        p = release.Package("test-package-depends-single-level-options-broken-a")
        rl = p.RelationshipOptionsList("depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert co.good.ReleaseMap()
        assert co.good.ReleaseMap()["sid"]
        assert not "unresolved" in co.good.ReleaseMap()

        # package with unresolvable option groups
        p = release.Package("test-package-depends-single-level-options-broken-b")
        rl = p.RelationshipOptionsList("depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert co.good.ReleaseMap()
        assert co.good.ReleaseMap()["sid"]
        assert not "unresolved" in co.good.ReleaseMap()

        # package with unresolvable deps
        p = release.Package("test-package-depends-single-unresolvable")
        rl = p.RelationshipOptionsList("depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert not co.good.ReleaseMap()
        assert co.bad.ReleaseMap()["unresolved"]
        assert co.bad.ReleaseMap()["unresolved"][0][0].package == "no-such-package"

        # package with unresolvable deps
        p = release.Package("test-package-depends-unresolvable")
        rl = p.RelationshipOptionsList("depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert co.good.ReleaseMap()
        assert co.good.ReleaseMap()["sid"]
        assert not "unresolved" in co.good.ReleaseMap()
        assert co.bad.ReleaseMap()["unresolved"]
        assert co.bad.ReleaseMap()["unresolved"][0][0].package == "no-such-package"

        # package with resolvable versioned deps
        p = release.Package("test-package-versioned-dependencies-resolvable")
        rl = p.RelationshipOptionsList("depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert co.good.ReleaseMap()
        assert co.good.ReleaseMap()["sid"]
        assert not "unresolved" in co.good.ReleaseMap()
        assert co.good.ReleaseMap()["sid"][0][0].package == "libtesta1"

        # package with unresolvable versioned deps
        p = release.Package("test-package-versioned-dependencies-unresolvable")
        rl = p.RelationshipOptionsList("depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert not co.good.ReleaseMap()
        assert not "unresolved" in co.good.ReleaseMap()
        assert co.bad.ReleaseMap()["unresolved"][0][0].package == "libtesta1"

        # all resolvable, virtual packages
        p = release.Package("test-package-virtual-dependencies")
        rl = p.RelationshipOptionsList("depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert not "unresolved" in co.good.ReleaseMap()
        assert "virtual" in co.good.ReleaseMap()

        # some arch-specific dependencies: check that the amd64 ones come through; armhf are filtered
        sp = release.Source("test-source-package-arch-specific-bd")
        rl = sp.RelationshipOptionsList("build_depends")
        co = checker.CheckRelationshipOptionsList(rl)
        assert co.good.ReleaseMap()
        assert co.good.ReleaseMap()["sid"]
        assert (
            co.good.ReleaseMap()["sid"][1][0].package == "test-package-sid-amd64-only"
        )
        assert "archignore" in co.good.ReleaseMap()
        assert co.good.ReleaseMap()["archignore"]
        assert (
            co.good.ReleaseMap()["archignore"][0][0].package
            == "test-package-sid-armhf-only"
        )

    def testPackageSet(self) -> None:
        """Test reducing a list of RelationshipOptions into list of packages"""
        # resolver not run so all relationships are unsatisified
        p = self.udd.BindPackage("test-package-all-releases", "sid", "amd64")
        rl = p.RelationshipOptionsList("depends")
        ps = rl.PackageSet()
        assert ps
        assert len(ps) == 2

        release = self.udd.BindRelease(arch="amd64", release="sid")
        checker = Checker(release)

        # list of checks with format (package name, good num, bad num)
        checks = [
            # package where everything is resolvable
            ("test-package-depends-single-level-a", 1, 0),
            # package where everything is resolvable (multiple deps)
            ("test-package-depends-single-level-b", 2, 0),
            # package where everything is resolvable multilevel deps
            ("test-package-depends-multilevel", 2, 0),
            # package with resolvable option groups
            ("test-package-depends-single-level-options", 2, 0),
            # package with unresolvable option groups
            ("test-package-depends-single-level-options-broken-a", 2, 0),
            # package with unresolvable option groups
            ("test-package-depends-single-level-options-broken-b", 2, 0),
            # package with unresolvable deps
            ("test-package-depends-single-unresolvable", 0, 1),
            # package with unresolvable deps
            ("test-package-depends-unresolvable", 1, 1),
            # package with resolvable versioned deps
            ("test-package-versioned-dependencies-resolvable", 1, 0),
            # package with unresolvable versioned deps
            ("test-package-versioned-dependencies-unresolvable", 0, 1),
            # all resolvable, virtual packages
            ("test-package-virtual-dependencies", 1, 0),
        ]

        for pkgname, good, bad in checks:
            p = release.Package(pkgname)
            rl = p.RelationshipOptionsList("depends")
            co = checker.CheckRelationshipOptionsList(rl)
            assert (
                len(co.good.PackageSet()) == good
            ), f"PackageSet good test for {pkgname}"
            assert len(co.bad.PackageSet()) == bad, f"PackageSet bad test for {pkgname}"

    def testStr(self) -> None:
        # non-existent package
        rl = RelationshipOptionsList()
        rl.append(RelationshipOptions("no-such-package"))
        assert str(rl)
        # FIXME check the value

        # list of packages
        release = self.udd.BindRelease(arch="amd64", release="sid")
        p = release.Package("test-package-all-releases")
        rl = p.RelationshipOptionsList("depends")
        assert str(rl)
        # FIXME check the value


class TestPackageLists:
    @pytest.fixture
    def pkgs(self) -> Generator[PackageLists, None, None]:
        l = PackageLists(["foo", "bar"])
        l.foo.append("1")
        l.foo.append("2")
        l.bar.append("quux")
        yield l

    def testInit(self) -> None:
        """Test creation of PackageLists lists"""
        assert not PackageLists([])
        assert not PackageLists(["foo", "bar"])
        assert not PackageLists(["foo", "bar"], RelationshipOptionsList)

    @no_type_check
    def testGetSet(self, pkgs: PackageLists) -> None:
        """Test getting and setting the values directly"""
        assert len(pkgs.foo) == 2
        assert len(pkgs.bar) == 1
        assert pkgs.bar[0] == "quux"

    def testIndirectNames(self, pkgs: PackageLists) -> None:
        """Test getting and setting the values indirectly"""
        assert len(pkgs.get("foo")) == 2
        assert len(pkgs.get("bar")) == 1
        assert pkgs.get("bar")[0] == "quux"
        pkgs.get("bar").append("goo")
        assert len(pkgs.get("bar")) == 2
        pkgs.set("bar", ["quux"])
        assert len(pkgs.get("bar")) == 1

    def testStr(self, pkgs: PackageLists) -> None:
        """Test string representation"""
        assert str(pkgs)
        assert len(str(pkgs)) >= 1

    def testNonzero(self, pkgs: PackageLists) -> None:
        """Test counting"""
        assert pkgs
        assert not PackageLists([])


@pytest.mark.usefixtures("udd")
class TestBuildDepStatus:
    udd: Udd

    def testInit(self) -> None:
        assert BuildDepStatus()
        assert BuildDepStatus(bd=RelationshipStatus(), bdi=RelationshipStatus())

    def testAllFound(self) -> None:
        release = self.udd.BindRelease(arch="amd64", release="sid")
        checker = Checker(release)

        # list of checks: source package name, status
        checks = [
            # all satisfiable (no bd)
            ("test-source-no-bd", True),
            # all satisfiable
            ("test-package-all-releases", True),
            # all satisfiable only bdi
            ("test-source-bdi", True),
            # all satisfiable arch specific
            ("test-source-package-arch-specific-bd", True),
            # unsatisfiable bd
            ("test-source-package-bd-unsatifiable", False),
            # unsatisfiable bdi
            ("test-source-package-bdi-unsatifiable", False),
            # unsatisfiable arch specific
            ("test-source-package-arch-specific-bd-unsatisfiable", False),
        ]

        for pkgname, bdallfound in checks:
            p = release.Source(pkgname)
            bdstatus = BuildDepStatus(
                bd=checker.CheckRelationshipOptionsList(p.BuildDependsList()),
                bdi=checker.CheckRelationshipOptionsList(p.BuildDependsIndepList()),
            )
            if bdallfound:
                assert bdstatus.AllFound(), f"BuildDep AllFound good test for {pkgname}"
            else:
                assert (
                    not bdstatus.AllFound()
                ), f"BuildDep AllFound bad test for {pkgname}"

    def testAllFoundMultiRelease(self) -> None:
        # test of backporting pinning/overlay releases
        # FIXME: hard coded release names are a problem here
        srelease = self.udd.BindRelease(arch="amd64")
        schecker = Checker(srelease)
        bporelease = self.udd.BindRelease(arch="amd64", release=[stable, stablebpo])
        bpochecker = Checker(bporelease)
        fromrelease = self.udd.BindRelease(arch="amd64", release="sid")

        checks = [
            ("test-source-package-backportable-no-bpo", schecker, True),
            ("test-source-package-backportable-no-bpo", bpochecker, True),
            ("test-source-package-backportable-bpo", schecker, False),
            ("test-source-package-backportable-bpo", bpochecker, True),
        ]

        for pkgname, checker, bdallfound in checks:
            p = fromrelease.Source(pkgname)
            bdstatus = BuildDepStatus(
                bd=checker.CheckRelationshipOptionsList(p.BuildDependsList()),
                bdi=checker.CheckRelationshipOptionsList(p.BuildDependsIndepList()),
            )
            if bdallfound:
                assert bdstatus.AllFound(), f"BuildDep AllFound good test for {pkgname}"
            else:
                assert (
                    not bdstatus.AllFound()
                ), f"BuildDep AllFound bad test for {pkgname}"

    def testReleaseMap(self) -> None:
        release = self.udd.BindRelease(arch="amd64", release="sid")
        checker = Checker(release)

        # list of checks: source package name, keys
        checks: List[Tuple[str, List[str]]] = [
            # all satisfiable (no bd)
            ("test-source-no-bd", []),
            # all satisfiable
            ("test-package-all-releases", ["sid"]),
            # all satisfiable only bdi
            ("test-source-bdi", ["sid"]),
            # all satisfiable arch specific
            ("test-source-package-arch-specific-bd", ["archignore", "sid"]),
            # unsatisfiable bd
            ("test-source-package-bd-unsatifiable", ["sid"]),
            # unsatisfiable bdi
            ("test-source-package-bdi-unsatifiable", ["sid"]),
            # unsatisfiable arch specific
            (
                "test-source-package-arch-specific-bd-unsatisfiable",
                ["archignore", "sid"],
            ),
            # virtual package
            ("test-source-package-bd-virtual", ["sid", "virtual"]),
        ]

        for pkgname, relsfound in checks:
            p = release.Source(pkgname)
            bdstatus = BuildDepStatus(
                bd=checker.CheckRelationshipOptionsList(p.BuildDependsList()),
                bdi=checker.CheckRelationshipOptionsList(p.BuildDependsIndepList()),
            )
            m = bdstatus.ReleaseMap()
            assert sorted(m.keys()) == sorted(
                relsfound
            ), f"ReleaseMap releases test for {pkgname}"
            # self.assertTrue(len(m['sid']) > 1, "ReleaseMap releases test for %s" % pkgname))

    def testStr(self) -> None:
        bdstatus = BuildDepStatus()
        assert str(bdstatus)

        release = self.udd.BindRelease(arch="amd64", release="sid")
        checker = Checker(release)
        p = release.Source("test-package-all-releases")
        bdstatus = BuildDepStatus(
            bd=checker.CheckRelationshipOptionsList(p.BuildDependsList()),
            bdi=checker.CheckRelationshipOptionsList(p.BuildDependsIndepList()),
        )
        s = str(bdstatus)
        assert s
        assert "Build-Depends" in s
        assert "libtesta1" in s


@pytest.mark.usefixtures("udd")
class TestRelationshipStatus:
    udd: Udd

    def testPackageSets(self) -> None:
        release = self.udd.BindRelease(arch="amd64", release="sid")
        s = RelationshipStatus()
        s.good.append(release.Package("test-package-all-releases").DependsList()[0])
        s.good.append(release.Package("test-package-all-releases").DependsList()[0])
        assert s.PackageSets()
        assert len(s.PackageSets().good) == 1
        assert len(s.PackageSets().bad) == 0
        # these packages have different first dependencies so they are not
        # going to be deduped
        s.good.append(release.Package("libtestxc1").DependsList()[0])
        s.bad.append(
            release.Package("test-package-depends-multilevel").DependsList()[0]
        )
        assert s.PackageSets()
        assert len(s.PackageSets().good) == 2
        assert len(s.PackageSets().bad) == 1

    def testExtend(self) -> None:
        s = RelationshipStatus()
        s.good.append(RelationshipOptions("pkg"))
        s2 = RelationshipStatus()
        s2.good.append(RelationshipOptions("pkg2"))
        s2.bad.append(RelationshipOptions("pkg3"))
        s.extend(s2)
        assert len(s.good) == 2
        assert len(s.bad) == 1

    def testSwap(self) -> None:
        s = RelationshipStatus()
        s.good.extend([RelationshipOptions("pkg1"), RelationshipOptions("pkg2")])
        assert s.satisfied()
        s.bad.extend([RelationshipOptions("pkg3")])
        assert not s.satisfied()
        s = RelationshipStatus()
        s.unchecked.extend([RelationshipOptions("pkg1"), RelationshipOptions("pkg2")])
        assert not s.satisfied()
        s = RelationshipStatus()
        s.bad.extend([RelationshipOptions("pkg1"), RelationshipOptions("pkg2")])
        assert not s.satisfied()

    def testSatisfied(self) -> None:
        s = RelationshipStatus()
        s.good.extend([RelationshipOptions("pkg1"), RelationshipOptions("pkg2")])
        s.bad.extend([RelationshipOptions("pkg3")])
        assert len(s.good) == 2
        assert len(s.bad) == 1
        s.swap()
        assert len(s.good) == 1
        assert len(s.bad) == 2

    def testStr(self) -> None:
        release = self.udd.BindRelease(arch="amd64", release="sid")
        s = RelationshipStatus()
        s.good.extend(release.Package("test-package-all-releases").DependsList())
        assert str(s)

        release = self.udd.BindRelease(arch="amd64", release="sid")
        s = RelationshipStatus()
        s.good.extend(release.Package("libtestxc1").DependsList())
        s.bad.extend(release.Package("test-package-all-releases").DependsList())
        s.unchecked.extend(
            release.Package("test-package-depends-multilevel").DependsList()
        )
        assert str(s)

        release = self.udd.BindRelease(arch="amd64", release="sid")
        s = RelationshipStatus()
        s.good.extend(release.Package("libtestxc1").DependsList())
        s2 = RelationshipStatus()
        s2.good.extend(release.Package("test-package-all-releases").DependsList())
        s.good[0].status = s2
        s3 = RelationshipStatus()
        s3.good.extend(release.Package("test-package-depends-multilevel").DependsList())
        s2.good[0].status = s3
        assert str(s)

    def testNonzero(self) -> None:
        s = RelationshipStatus()
        assert not s
        s.good.append(RelationshipOptions("pkg"))
        assert s
        s = RelationshipStatus()
        s.bad.append(RelationshipOptions("pkg"))
        assert s
        s = RelationshipStatus()
        s.unchecked.append(RelationshipOptions("pkg"))
        assert s
        s = RelationshipStatus()
        s.good.append(RelationshipOptions("pkg"))
        s.bad.append(RelationshipOptions("pkg"))
        s.unchecked.append(RelationshipOptions("pkg"))
        assert s


class MockPackage:
    def __init__(self, name: str) -> None:
        self.package = name


class TestDepends:
    def testInit(self) -> None:
        d = Depends(MockPackage("foo"))  # type: ignore
        assert d
        assert d.packagedata.package == "foo"

    def test_str_conversion(self) -> None:
        d = Depends(MockPackage("foo"))  # type: ignore
        assert "foo" in str(d)


class TestRecommends:
    def testInit(self) -> None:
        d = Recommends(MockPackage("foo"))  # type: ignore
        assert d
        assert d.packagedata.package == "foo"

    def test_str_test_str_conversion(self) -> None:
        d = Recommends(MockPackage("foo"))  # type: ignore
        assert "foo" in str(d)


class TestDependencyChain:
    @no_type_check
    def testInit(self) -> None:
        assert not DependencyChain()
        assert DependencyChain(relation=Depends("a"), base="z")
        assert DependencyChain(chain=[Depends("a"), Depends("b")])
        assert len(DependencyChain(chain=[Depends("a"), Depends("b")])) == 2

    @no_type_check
    def testTruncated(self) -> None:
        c = DependencyChain(
            chain=[Depends(MockPackage(x)) for x in ["a", "b", "c", "d", "e"]]
        )
        assert len(c.truncated("a")) == 1
        assert len(c.truncated("d")) == 4
        assert len(c.truncated("f")) == 0

    @no_type_check
    def testContains(self) -> None:
        c = DependencyChain(
            chain=[Depends(MockPackage(x)) for x in ["a", "b", "c", "d", "e"]]
        )
        assert c.contains("a")
        assert not c.contains("f")

    @no_type_check
    def testDistance(self) -> None:
        c = DependencyChain(
            chain=[Depends(MockPackage(x)) for x in ["a", "b", "c", "d", "e"]]
        )
        assert c.distance() == 5
        c = DependencyChain(
            chain=[Recommends(MockPackage(x)) for x in ["a", "b", "c", "d", "e"]]
        )
        assert c.distance() == 5000
        c = DependencyChain(
            chain=[
                Depends(MockPackage("a")),
                Recommends(MockPackage("b")),
                Depends(MockPackage("c")),
            ]
        )
        assert c.distance() == 1002

    @no_type_check
    def test_str_conversion(self) -> None:
        names = ["aaa", "bbbb", "ccccc", "ddddddd", "eee"]
        c = DependencyChain(chain=[Depends(MockPackage(x)) for x in names])
        for n in names:
            assert n in str(c)
        c.base = "test"
        assert "test" in str(c)


class TestDependencyChainList:
    @no_type_check
    def testUnique(self) -> None:
        names = ["aaa", "bbbb", "ccccc", "ddddddd", "eee", "ff", "ggggg"]
        cl = DependencyChainList(
            [
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[3:5]]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[:4]]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[3:5]]),
            ]
        )
        assert len(cl) == 4
        assert len(cl.unique()) == 3

    @no_type_check
    def testTruncated(self) -> None:
        names = ["aaa", "bbbb", "ccccc", "ddddddd", "eee", "ff", "ggggg"]
        cl = DependencyChainList(
            [
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[3:5]]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[:4]]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[3:5]]),
            ]
        )
        assert len(cl.truncated("ccccc")) == 2

    @no_type_check
    def testSorted(self) -> None:
        names = ["aaa", "bbbb", "ccccc", "ddddddd", "eee", "ff", "ggggg"]
        cl = DependencyChainList(
            [
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[3:5]]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[:4]]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[3:5]]),
            ]
        )
        cls = cl.sorted()
        assert len(cls[0]) == 2
        assert len(cls[3]) == 7

        cl.append(
            DependencyChain(
                chain=[
                    Depends(MockPackage("a")),
                    Recommends(MockPackage("b")),
                    Depends(MockPackage("c")),
                ]
            )
        )
        cls = cl.sorted()
        assert len(cls[0]) == 2
        assert len(cls[4]) == 3

    @no_type_check
    def testSetBase(self) -> None:
        names = ["aaa", "bbbb", "ccccc", "ddddddd", "eee", "ff", "ggggg"]
        cl = DependencyChainList(
            [
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[3:5]]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[:4]]),
                DependencyChain(chain=[Depends(MockPackage(x)) for x in names[3:5]]),
            ]
        )
        cl.set_base("foo")
        for c in cl:
            assert c.base == "foo"
