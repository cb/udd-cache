# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Unit tests for base UDD bindings """

import psycopg
import pytest

from uddcache.udd import Udd
from uddcache.config import Config


@pytest.mark.usefixtures("postgresql_direct_access")
class TestDatabase:
    postgresql: psycopg.Connection

    def testDBTypes(self) -> None:
        """Test creating a Debian and derivative UDD instance"""
        pg = self.postgresql
        assert Udd(distro="debian", db=pg)
        with pytest.raises(NotImplementedError):
            Udd(distro="ubuntu", db=pg)  # TODO: update when implemented
        with pytest.raises(ValueError):
            Udd(distro="nosuchdistro", db=pg)

    def testPassConfig(self) -> None:
        """Test loading a config file manually"""
        config = Config()
        assert Udd(config=config, db=self.postgresql)


@pytest.mark.usefixtures("udd")
class TestDatabaseBinding:
    udd: Udd

    def testRelease(self) -> None:
        """Test binding to a release and doing a lookup"""
        r = self.udd.BindRelease("sid")
        assert r
        r = self.udd.BindRelease(["stable", "stable-backports"])
        assert r

    def testPackage(self) -> None:
        """Test binding to a binary package and doing a lookup"""
        r = self.udd.BindPackage("test-package-all-releases", "sid", "amd64")
        assert r
        assert r.Found()
        r = self.udd.BindPackage("test-package-sid-only", None, "amd64")
        assert r
        assert not r.Found()
        r = self.udd.BindPackage("test-package-stable-only", None, "amd64")
        assert r
        assert r.Found()
        r = self.udd.BindPackage("test-package-sid-armhf-only", "sid", "armhf")
        assert r
        assert r.Found()
        r = self.udd.BindPackage("test-package-sid-armhf-only", "sid", "amd64")
        assert r
        assert not r.Found()

    def testSource(self) -> None:
        """Test binding to a source package and doing a lookup"""
        r = self.udd.BindSourcePackage("test-package-all-releases", "sid")
        assert r
        assert r.Found()
        r = self.udd.BindSourcePackage("test-package-all-releases", "sid")
        assert r
        assert r.Found()
        assert r.data
        assert r.data["version"]

    def testBts(self) -> None:
        """Test binding to a source package and doing a lookup"""
        tracker = self.udd.Bts(False)
        assert not tracker.include_archived
