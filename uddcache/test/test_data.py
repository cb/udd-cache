# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Unit tests for data """

# Fairly sparse tests at this stage

from uddcache.data import DebianData


class TestData:
    def testDebian(self) -> None:
        """Test switching to Debian data sets"""
        assert DebianData.release_map["unstable"] == "sid"
        assert "i386" in DebianData.archs
        assert "sid" in DebianData.releases

    def testReleaseName(self) -> None:
        """Test manipulations of the release names"""
        assert DebianData.clean_release_name("sid") == "sid"
        assert DebianData.clean_release_name("unstable") == "sid"
        assert DebianData.clean_release_name("nosuchrelease", default="quux") == "quux"
        assert DebianData.clean_release_name(args=["a", "sid", "argument"]) == "sid"
        assert (
            DebianData.clean_release_name(
                args=["a", "nonsid", "argument"], default="quux"
            )
            == "quux"
        )
        assert (
            DebianData.clean_release_name(
                optlist=[("option", "value"), ("release", "sid")]
            )
            == "sid"
        )
        assert (
            DebianData.clean_release_name(optlist=[("option", "value")], default="quux")
            == "quux"
        )
        assert (
            DebianData.clean_release_name(
                optlist=[("want", "sid"), ("release", "stable")], optname="want"
            )
            == "sid"
        )

    def testDependentRelease(self) -> None:
        """Test finding dependent releases"""
        # pylint: disable=use-implicit-booleaness-not-comparison
        stable = DebianData.stable_release
        assert DebianData.list_dependent_releases("nosuchrelease") == []
        assert DebianData.list_dependent_releases("sid") == ["sid"]
        assert DebianData.list_dependent_releases(stable) != ["sid"]
        assert sorted(
            DebianData.list_dependent_releases(f"{stable}-backports")
        ) == sorted([f"{stable}-backports", stable])
        assert DebianData.list_dependent_releases(
            f"{stable}-backports", include_self=False
        ) == [stable]
        assert sorted(
            DebianData.list_dependent_releases(
                stable, suffixes=["security", "backports"]
            )
        ) == sorted([stable, f"{stable}-security", f"{stable}-backports"])
        assert DebianData.list_dependent_releases(
            "sid", suffixes=["security", "backports"]
        ) == ["sid"]
        # experimental is an overlay to sid
        assert DebianData.list_dependent_releases("experimental") == [
            "experimental",
            "sid",
        ]
        # also support release names
        assert sorted(DebianData.list_dependent_releases("stable-backports")) == sorted(
            [f"{stable}-backports", stable]
        )

    def testArchName(self) -> None:
        """Test manipulations of the arch names"""
        assert DebianData.clean_arch_name("amd64") == "amd64"
        assert DebianData.clean_arch_name("nosucharch", default="hppa") == "hppa"
        assert (
            DebianData.clean_arch_name(args=["a", "powerpc", "argument"]) == "powerpc"
        )
        assert (
            DebianData.clean_arch_name(
                args=["a", "non-i386", "argument"], default="quux"
            )
            == "quux"
        )
        assert (
            DebianData.clean_arch_name(
                optlist=[("option", "value"), ("arch", "powerpc")]
            )
            == "powerpc"
        )
        assert (
            DebianData.clean_arch_name(optlist=[("option", "value")], default="quux")
            == "quux"
        )
        assert (
            DebianData.clean_arch_name(
                optlist=[("want", "amd64"), ("arch", "hppa")], optname="want"
            )
            == "amd64"
        )
