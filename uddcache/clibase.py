# coding: utf-8

#
# Ultimate Debian Database query tool
#
# CLI bindings
#
###
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Base class for command line interface to udd - output to stdout """

import argparse
from typing import Any, Callable, List, Optional, Union

from . import udd
from .config import Config, FileLocationType


CommandArgsType = List[str]


class CliBase:
    """Run a specified command sending output to stdout"""

    def __init__(
        self,
        config: Optional[Union[FileLocationType, Config]] = None,
        options: Any = None,
        initialise: bool = True,
        dispatcherClass: Optional[Callable[[udd.Udd], Any]] = None,
        uddInstance: Optional[udd.Udd] = None,
    ) -> None:
        if not options:
            raise ValueError("No options specified.")
        if initialise:
            if uddInstance:
                self.udd = uddInstance
            else:
                self.udd = udd.Udd(config=config, distro=options.distro)
            self.dispatcher = dispatcherClass(self.udd)  # type: ignore
            self.options = options

    @staticmethod
    def parse_args(argv: Optional[List[str]] = None) -> argparse.Namespace:
        raise NotImplementedError()  # pragma: no cover

    @classmethod
    def main(
        cls, argv: Optional[List[str]] = None, uddInstance: Optional[udd.Udd] = None
    ) -> bool:
        args = cls.parse_args(argv)

        runner = cls(config=args.config, options=args, uddInstance=uddInstance)

        success = args.func(runner, args)
        return not success


main = CliBase.main
